const express = require("express");
const router = express.Router();
const authController = require("../controllers/auth.js");

router.post("/login", authController.authenticate);
router.post("/register", authController.register);
router.get("/verify", authController.verify);
router.post("/password", authController.newPassword);

module.exports = router;
