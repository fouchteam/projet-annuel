const express = require('express');
const router = express.Router();
const StreamController = require("../controllers/streams");

router.get("/", StreamController.getList);
router.get("/:username", StreamController.findByUsername);

module.exports = router;