const express = require("express");
const router = express.Router();
const userController = require("../controllers/user.js");

router.get("/", userController.findAll);
router.get("/:id", userController.findOne);
router.get("/username/:username", userController.findOneByUsername);
router.get("/search/username/:username", userController.findByLikeUsername);

router.put("/:id", userController.update);
router.delete("/:id", userController.remove);

router.post('/stream_key', userController.generateKeyStream);

module.exports = router;
