const express = require("express");
const router = express.Router();
const followController = require("../controllers/follow.js");

router.post("/:id", followController.store);
router.get("/followers/:id", followController.getUserFollower);
router.get("/followed/:id", followController.getUserFollowed);
router.delete("/:id", followController.destroy);

module.exports = router;
