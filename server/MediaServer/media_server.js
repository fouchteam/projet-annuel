const User = require("../models/user.js");
const NodeMediaServer = require('node-media-server');
const config = require('./config/default').rtmp_server;
const helpers = require("./helper");
const thumbnail_generator = require('./cron/thumbnails');

const nms = new NodeMediaServer(config);

nms.on('prePublish', (id, StreamPath) => {
    const stream_key = getStreamKeyFromStreamPath(StreamPath);

    User.findOne({ stream_key: stream_key }, "nickname description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr")
    .populate({
        path: "followers",
        select: "follower",
        populate: { path: "follower", select: "nickname display_name url_profile_picture email" },
    }).exec((err, user) => {
        if (!err) {
            if (!user) {
                rejectSession(id);
            } else {
                helpers.sendNotificationsToUserFollowers(user.nickname, user.followers);
                helpers.generateStreamThumbnail(stream_key);
            }
        }
    });
});
const getStreamKeyFromStreamPath = (path) => {
    let parts = path.split('/');
    return parts[parts.length - 1];
};

const rejectSession = (id) => {
    let session = nms.getSession(id);
    session.reject();
}


// Call start method at the end of file
thumbnail_generator.start();

module.exports = nms;
