const dotenv = require('dotenv');
const nodemailer = require('nodemailer');

dotenv.config();

/*
    STMP details
*/
var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env.GMAIL_USER,
        pass: process.env.GMAIL_PASS
    }
});
/*------------------SMTP Over-----------------------------*/

const spawn = require('child_process').spawn,
    config = require('./config/default'),
    cmd = config.rtmp_server.trans.ffmpeg;

const generateStreamThumbnail = (stream_key) => {
    const args = [
        '-y',
        '-i', (process.env.NODE_ENV == "production" && process.env.HOME_DIRECTORY ? process.env.HOME_DIRECTORY + "/" : "") + 'server/media/live/' + stream_key + '/index.m3u8',
        '-vframes', '1',
        '-vf', 'scale=-2:300',
        (process.env.NODE_ENV == "production" && process.env.HOME_DIRECTORY ? process.env.HOME_DIRECTORY + "/" : "") + 'server/thumbnails/' + stream_key + '.png',
    ];
    const subprocess = spawn(cmd, args, {
        detached: true,
        stdio: ['ignore']
    });
    subprocess.on('error', () => {
        console.log(`[LOG][ERROR] Impossible de générer le thumbnail pour ${stream_key}`);
    });
    // subprocess.stdout.on('data', stdout => {
    //     console.log("stdout : \n"+stdout);
    // });
    // subprocess.stderr.on("data", stderr => {
    //     console.log("stderr : \n"+stderr);
    // })
    // subprocess.on('close', (code) => {
    //     console.log(code);
    //     if (code !== 0) {
    //         console.log(`ps process exited with code ${code}`);
    //     }
    // });
    subprocess.unref();
    console.log(`[LOG] Thumbnail Generation for ${stream_key}`);
};

const sendNotificationsToUserFollowers = (nickname, followers) => {
    followers.map(follower => {
        const link = process.env.REACT_APP_URL + 'stream/' + nickname;
        let mailOptions = {
            to: follower.follower.email,
            subject: "[TUNEL] "+nickname+" is streaming !",
            html: "Hello,<br> Your followed streamer " + nickname + " has just started streaming !<br><a href=" + link + " target=\"_blank\">Go to the stream</a>"
        }
        try {
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                } else {
                    console.log("mail sent to "+follower.follower.email);
                }
            });
        } catch (e) {
            console.log(e);
        }
    });
};

module.exports = {
    generateStreamThumbnail,
    sendNotificationsToUserFollowers
};
