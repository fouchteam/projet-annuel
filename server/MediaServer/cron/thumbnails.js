const CronJob = require('cron').CronJob;
const StreamController = require("../../controllers/streams");
const {generateStreamThumbnail} = require("../helper");

const job = new CronJob('*/5 * * * * *', async function () {
    const live_streams = await StreamController.getStreamingUsers();
    if (live_streams.length > 0) {
        live_streams.map(item => {
            generateStreamThumbnail(item.stream_key);
        })
    }
}, null, true);

module.exports = job;