const User = require("../models/user.js");
const mongoose = require('mongoose');

const app = require("../app"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);


describe("User integration test", () => {
  // Connection
  let db;

  // Credentials
  let EMAIL = "test_user@test.fr"
  let NICKNAME = "test_user";
  let PASSWORD = "test";

  var SAVED_USER;
  var SAVED_USER_BIS;
  var TOKEN;

  beforeAll(async () => {
    const URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo`
    const DB_NAME = "jest"

    let connection = await mongoose.connect(
      process.env.MONGODB_URI || URI,
      {
        dbName: DB_NAME,
        useCreateIndex: true,
        useNewUrlParser: true,
      },
      (err) => {
        if (err) {
          process.exit(1);
        }
      }
    );

    db = mongoose.connection;
  });

  afterAll(async () => {
    await db.collection('follows').deleteMany({});
    await db.collection('users').deleteMany({});
  });

  beforeEach(async () => {
    await db.collection('users').deleteMany({});

    // User 1
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    SAVED_USER = await initialUser.register()

    // User 1
    let initialUserBis = new User({
      email: "bis" + EMAIL,
      password: PASSWORD,
      nickname: "bis" + NICKNAME
    });
    SAVED_USER_BIS = await initialUserBis.register()

    // Auth user 1
    await request.get("/verify?id=" + SAVED_USER.verification_id);
    const response = await request.post("/login").send({
      email: EMAIL,
      password: PASSWORD
    });
    TOKEN = response.body.token
  });

  afterEach(async () => {
    await db.collection('users').deleteMany({});
  });

  it('Should modify the user', async (done) => {
    // PUT
    const response = await request.put("/users/" + SAVED_USER._id).send({
      nickname: NICKNAME + "1"
    }).set({ Authorization: "Bearer " + TOKEN });
    expect(response.status).toBe(200);
    expect(response.body.newUser.nickname).toBe(NICKNAME + "1");

    done();
  });

  it('Should not modify the user', async (done) => {
    // PUT
    const response = await request.put("/users/" + SAVED_USER_BIS._id).send({
      nickname: NICKNAME + "1"
    }).set({ Authorization: "Bearer " + TOKEN });
    expect(response.status).toBe(403);

    done();
  });

  it('Should change stream key', async (done) => {
    const response = await request.post("/users/stream_key").send({ _id: SAVED_USER._id }).set({ Authorization: "Bearer " + TOKEN });
    expect(response.status).toBe(200);

    done()
  });

  it('Should not change stream key', async (done) => {
    const response = await request.post("/users/stream_key").send({ _id: SAVED_USER_BIS._id }).set({ Authorization: "Bearer " + TOKEN });
    expect(response.status).toBe(403);

    done()
  });

});