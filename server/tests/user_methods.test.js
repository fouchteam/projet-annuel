const User = require("../models/user.js");
const mongoose = require('mongoose');

describe("Users tests methods", () => {
  // Connection
  let db;

  // Credentials
  let EMAIL = "test@test.fr";
  let EMAIL_2 = "test2@test.fr";
  let NICKNAME = "test";
  let NICKNAME_2 = "test2";
  let PASSWORD = "test";

  let ERROR_DUPLICATED_EMAIL = "User email already exists"
  let ERROR_DUPLICATED_NICKNAME = "User nickname already exists"

  beforeAll(async () => {
    const URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo`
    const DB_NAME = "jest"

    let connection = await mongoose.connect(
      process.env.MONGODB_URI || URI,
      {
        dbName: DB_NAME,
        useCreateIndex: true,
        useNewUrlParser: true,
      },
      (err) => {
        if (err) {
          process.exit(1);
        }
      }
    );

    db = mongoose.connection;
  });

  beforeEach(async () => {
    await db.collection('users').deleteMany({});
  });

  it('Should register the user', async () => {

    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    const data = await initialUser.register()
    expect(data.email).toBe(EMAIL);
    expect(data.password).not.toBe(PASSWORD);
    expect(data.nickname).toBe(NICKNAME);
  });

  it('Should register two users', async () => {

    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    let initialUserBis = new User({
      email: EMAIL_2,
      password: PASSWORD,
      nickname: NICKNAME_2
    });

    const data = await initialUser.register()
    expect(data.email).toBe(EMAIL);
    expect(data.password).not.toBe(PASSWORD);
    expect(data.nickname).toBe(NICKNAME);

    const dataBis = await initialUserBis.register()
    expect(dataBis.email).toBe(EMAIL_2);
    expect(dataBis.password).not.toBe(PASSWORD);
    expect(dataBis.nickname).toBe(NICKNAME_2);

  });

  it('Should not register the user cause of nickname and email duplication', async () => {
    const user = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    const user2 = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    try {
      await user.register()
      await user2.register()
    } catch (err) {
      expect(err).toBe(ERROR_DUPLICATED_EMAIL);
    }
  });

  it('Should not register the user cause of email duplication', async () => {
    const user = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    const user2 = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME_2
    });

    try {
      await user.register()
      await user2.register()
    } catch (err) {
      expect(err).toBe(ERROR_DUPLICATED_EMAIL);
    }
  });

  it('Should not register the user cause of nickname duplication', async () => {
    const user = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });

    const user2 = new User({
      email: EMAIL_2,
      password: PASSWORD,
      nickname: NICKNAME
    });

    try {
      await user.register()
      await user2.register()
    } catch (err) {
      expect(err).toBe(ERROR_DUPLICATED_NICKNAME);
    }
  });

  it('Should find by credentials the user', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    const data = await initialUser.register()

    // Find
    const user = await User.findByCredentials(EMAIL, PASSWORD)
    expect(data.email).toBe(user.email);
  });

  it('Should not find by crendentials the user', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    await initialUser.register()

    // Find
    try {
      const user = await User.findByCredentials(EMAIL, PASSWORD + "AAAA")
    } catch (e) {
      expect(e).toBe("Invalid credentials")
    }
  });

  it('Should mail should exist', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    await initialUser.register()

    // Find
    const response = await User.existingEmail(EMAIL)
    expect(response).toBe(true)
  });

  it('Should mail not should exist', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    await initialUser.register()

    // Find
    const response = await User.existingEmail(EMAIL_2)
    expect(response).toBe(false)
  });

  it('Should nickname should exist', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    await initialUser.register()

    // Find
    const response = await User.existingNickname(NICKNAME)
    expect(response).toBe(true)
  });

  it('Should nickname not should exist', async () => {

    // Register a user
    let initialUser = new User({
      email: EMAIL,
      password: PASSWORD,
      nickname: NICKNAME
    });
    await initialUser.register()

    // Find
    const response = await User.existingNickname(NICKNAME_2)
    expect(response).toBe(false)
  });

});