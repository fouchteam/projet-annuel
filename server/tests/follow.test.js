const User = require("../models/user.js");
const mongoose = require('mongoose');

const app = require("../app"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);


describe("Follow integration tests", () => {
    // Connection
    let db;

    // Credentials
    let EMAIL = "test_follow@test.fr"
    let NICKNAME = "test_follow";
    let PASSWORD = "test";

    let SAVED_USER;
    let SAVED_USER_BIS;

    let TOKEN;

    // BEFORE ALL TESTS
    beforeAll(async () => {
        // Mongo DB
        const URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo`
        const DB_NAME = "jest"
        await mongoose.connect(
            process.env.MONGODB_URI || URI,
            {
                dbName: DB_NAME,
                useCreateIndex: true,
                useNewUrlParser: true,
            },
            (err) => {
                if (err) {
                    process.exit(1);
                }
            }
        );
        db = mongoose.connection;
    });

    // AFTER ALL TESTS
    afterAll(async () => {
        await db.collection('follows').deleteMany({});
        await db.collection('users').deleteMany({});
    });

    // Register and Login ok
    it('Should follow, get and unfollow scenario', async (done) => {

        // User 1
        let initialUser = new User({
            email: EMAIL,
            password: PASSWORD,
            nickname: NICKNAME
        });
        SAVED_USER = await initialUser.register()

        // User 2
        let initialUserBis = new User({
            email: "bis" + EMAIL,
            password: PASSWORD,
            nickname: "bis" + NICKNAME
        });
        SAVED_USER_BIS = await initialUserBis.register()

        // Auth user 1
        await request.get("/verify?id=" + SAVED_USER.verification_id);
        const response = await request.post("/login").send({
            email: EMAIL,
            password: PASSWORD
        });
        TOKEN = response.body.token

        const follow_resp_1 = await request.post("/follow/5f0e3c1ceb30a30af2d113a4").set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_1.status).toBe(400);
        expect(follow_resp_1.body.message).toBe("The account you want to follow doesn't exist");

        const follow_resp_2 = await request.post("/follow/" + SAVED_USER._id).set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_2.status).toBe(400);
        expect(follow_resp_2.body.message).toBe("You can't follow yourself");

        const follow_resp_3 = await request.post("/follow/" + SAVED_USER_BIS._id).set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_3.status).toBe(200);
        expect(follow_resp_3.body.message).toBe("Successfully followed user");

        const follow_resp_4 = await request.post("/follow/" + SAVED_USER_BIS._id).set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_4.status).toBe(400);
        expect(follow_resp_4.body.message).toBe("You are already following this account");

        const follow_resp_5 = await request.delete("/follow/" + SAVED_USER_BIS._id).set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_5.status).toBe(204);

        const follow_resp_6 = await request.delete("/follow/" + SAVED_USER_BIS._id).set({ Authorization: "Bearer " + TOKEN }).send({})
        expect(follow_resp_6.status).toBe(404);
        expect(follow_resp_6.body.message).toBe("You can't unfollow someone that you are not following");

        done();
    });

});