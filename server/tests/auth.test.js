const User = require("../models/user.js");
const mongoose = require('mongoose');

const app = require("../app"); // Link to your server file
const supertest = require("supertest");
const request = supertest(app);


describe("Auth integration tests", () => {
  // Connection
  let db;
  // Credentials
  let EMAIL = "test_auth@test.fr"
  let NICKNAME = "test_auth";
  let PASSWORD = "test";

  // Before all tests
  beforeAll(async () => {
    const URI = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASSWORD}@mongo`
    const DB_NAME = "jest"

    await mongoose.connect(
      process.env.MONGODB_URI || URI,
      {
        dbName: DB_NAME,
        useCreateIndex: true,
        useNewUrlParser: true,
      },
      (err) => {
        if (err) {
          process.exit(1);
        }
      }
    );

    db = mongoose.connection;
    await db.collection('users').deleteMany({});
  });

  // AFTER ALL TESTS
  afterAll(async () => {
    await db.collection('users').deleteMany({});
  });

  // Before each tests
  beforeEach(async () => {
    await db.collection('users').deleteMany({});
  });

  // Register and Login ok
  it('Should register verify and login the user', async (done) => {

    // Register
    const response = await request.post("/register").send({
      email: EMAIL,
      nickname: NICKNAME,
      password: PASSWORD
    });
    expect(response.status).toBe(201);
    expect(response.body.id).not.toBeNull();
    expect(response.body.email).toBe(EMAIL);
    expect(response.body.nickname).toBe(NICKNAME);

    // Store user
    let user = await User.findOne({ _id: response.body.id })

    // Login unverified
    const response_ = await request.post("/login").send({
      email: EMAIL,
      password: PASSWORD
    });
    expect(response_.status).toBe(400);
    expect(response_.body.error).toBe("Account not verified !");

    // Verify
    await request.get("/verify?id=" + user.verification_id);

    // Login verified
    const response___ = await request.post("/login").send({
      email: EMAIL,
      password: PASSWORD
    });

    expect(response___.status).toBe(200);
    expect(response___.body.token).not.toBeNull();

    done();
  });

});