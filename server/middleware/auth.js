const verifyToken = require("../lib/jwt").verifyToken;

const verify = (req, res, next) => {
  const authHeader = req.get("Authorization");
  if (!authHeader || !authHeader.startsWith("Bearer")) {
    req.user = { id: 0 };
    next();
  } else {
    const token = authHeader.replace("Bearer ", "");
    verifyToken(token)
      .then((decodedToken) => {
        req.user = decodedToken;
        next();
      })
      .catch((_) => {
        req.user = { id: 0 };
        next();
      });
  }
};

module.exports = verify;
