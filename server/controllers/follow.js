const User = require("../models/user");
const Follow = require("../models/follow");
const { readyState } = require("../lib/db");

var exports = (module.exports = {});

/**
 * Save a follow
 *
 * @param {*} req Request
 * @param {*} res Response
 */
exports.store = async (req, res) => {
  let follower = req.user.id;
  let followee = req.params.id;

  // 1. On ne peut pas s'auto follow
  if (follower === followee)
    return res.status(400).json({ message: "You can't follow yourself" });

  let follow = new Follow({
    follower: follower,
    followee: followee,
  });

  // 2. Chercher si on le follow pas deja
  const findFollow = await Follow.findOne({
    follower: follower,
    followee: followee,
  });

  if (findFollow)
    return res.status(400).json({ message: "You are already following this account" });

  // 3. Utilisateur non trouvé
  const findFollowee = await User.findOne({
    _id: followee,
  });
  if (!findFollowee)
    return res
      .status(400)
      .json({ message: "The account you want to follow doesn't exist" });

  try {
    const result = follow.save();
    // 5. OK
    return res.status(200).json({
      message: "Successfully followed user",
    });
  } catch (e) {
    console.error(e);
    return res.status(404).json({
      message: "There was an error trying follow the user.",
    });
  }
};

/**
 * Method to get follower for one user
 *
 * @param {*} id corresponds to a Long
 */
exports.getUserFollower = async (req, res) => {
  try {
    const result = await Follow.find({ followee: req.params.id }, "follower")
      .populate({
        path: "follower",
        select: "nickname display_name url_profile_picture url_banner",
      })
    res.status(200).send(result);
  } catch (e) {
    console.error(e)
    res.status(500).send(e);
  }
};

/**
 * Method to get followed for one user
 *
 * @param {*} id corresponds to a Long
 */
exports.getUserFollowed = async (req, res) => {
  try {
    const result = await Follow.find({ follower: req.params.id }, "followee")
      .populate({
        path: "followee",
        select: "nickname display_name url_profile_picture url_banner",
      })
    res.status(200).send(result);
  } catch (e) {
    console.error(e)
    res.status(500).send(e);
  }
};

/**
 * Destroy
 *
 * @param {*} req Request
 * @param {*} res Response
 */
exports.destroy = async (req, res) => {
  let follower = req.user.id;
  let followee = req.params.id;

  // 2. Chercher si on le follow pas deja
  const findFollow = await Follow.findOne({
    follower: follower,
    followee: followee,
  });

  if (!findFollow)
    return res.status(404).json({
      message: "You can't unfollow someone that you are not following",
    });

  try {
    await Follow.deleteOne({ follower: follower, followee: followee });
    return res.status(204).send();
  } catch (e) {
    console.error(e)
    return res.status(500).json({
      message: "Error removing record",
    });
  }
};

// ------------------------------------------------------
// --------------------- METHODS ------------------------
// ------------------------------------------------------

/**
 * Method to get follower for one user
 *
 * @param {*} id corresponds to a Long
 */
exports.getFollowerForOneUser = async (id) => {
  try {
    return await Follow.findOne({ followee: id });
  } catch (e) {
    throw e;
  }
};

/**
 * Method to get followed for one user
 *
 * @param {*} id coresponds to a Long
 */
exports.getFollowedForOneUser = async (id) => {
  try {
    return await Follow.findOne({ follower: id });
  } catch (e) {
    throw e;
  }
};
