const fetch = require("node-fetch");
const fs = require("fs");
const User = require("../models/user");
const { encodeBase64 } = require("bcryptjs");
let base64 = require('base-64');

function getThumbnailForUsers(users) {
    return users.map(item => {
        let base64data = "";
        try {
            const buff = fs.readFileSync((process.env.NODE_ENV === "production" && process.env.HOME_DIRECTORY ? process.env.HOME_DIRECTORY : "/app") + `/server/thumbnails/${item.stream_key}.png`);
            base64data = buff.toString('base64');
        } catch (e) {
        }
        item = {
            ...item._doc,
            thumbnail: base64data
        };
        return item;
    });
}

const getList = async (req, res) => {
    try {
        const users = await getStreamingUsers();
        if (users.length > 0) {
            const resp = await getThumbnailForUsers(users);
            if (resp) {
                return res.json(resp);
            }
        }
        return res.json([]);
    } catch (e) {
        return res.status(500).json({ error: e.message });
    }
};

const getStreamingUsers = async () => {

    let headers = new fetch.Headers();
    let username = 'tunel-app';
    let password = (process.env.NODE_ENV === "production" ? process.env.PASSWORD_MS : 'devfouchteam')
    headers.set('Authorization', 'Basic ' + base64.encode(username + ":" + password));

    try {
        const resp = await fetch((process.env.NODE_ENV === "production" ? 'https' : "http") + "://" + process.env.HOSTNAME + ":8888/api/streams", {
            method: 'GET',
            headers: headers,
        });
        const json = await resp.json();
        if (json.live) {
            const streams = deleteNotValidStream(json.live);
            let query = { $or: [] };
            for (let stream in streams) {
                if (!streams.hasOwnProperty(stream)) continue;
                query.$or.push({ stream_key: stream });
            }
            if (query.$or.length === 0) {
                return [];
            }
            return await User.find(query, "nickname stream_key description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr");
        }
        return [];
    }
    catch (e) { console.error(e) }
};

const deleteNotValidStream = live => {
    //on retire les streams qui n'ont pas été publié (donc pas de clé de stream valide) dans le back
    Object.keys(live).map(i => {
        if (!live[i].publisher) {
            delete live[i];
        }
    });
    return live;
};

const findByUsername = async (req, res) => {
    const username = req.params.username;
    const user = await User.findOne({
        nickname: username
    }, "nickname description stream_key url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr");
    if (!user) {
        return res.status(404).json({ message: `User "${username}" don't exist` })
    }
    let isStreaming = true;
    const streampath = ((process.env.NODE_ENV === "production" && process.env.HOME_DIRECTORY ? process.env.HOME_DIRECTORY : "/app") + `/server/media/live/${user.stream_key}/index.m3u8`);
    try {
        await fs.readFileSync(streampath);
    } catch (e) {
        isStreaming = false;
    }
    if (!isStreaming) {
        delete user._doc.stream_key;
    }
    return res.json({
        ...user._doc,
        isStreaming,
        streamPath: isStreaming ? streampath : null
    });
};

module.exports = {
    getList,
    getStreamingUsers,
    findByUsername
};
