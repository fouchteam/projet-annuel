const User = require("../models/user");
const Follow = require("../models/follow");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs");

var exports = (module.exports = {});

// FIND ALL
exports.findAll = (req, res) => {
  User.find(
    {},
    "nickname description url_profile_picture url_banner display_name stream_name stream_description tags"
  )
    .then((data) => {
      res.status(200).json(data);
    })
    .catch((err) => {
      console.error(err);
      res.status(400).json({ error: err });
    });
};

// FIND ONE
exports.findOne = (req, res) => {
  let id = req.params.id;

  let select;
  if (req.user.id === req.params.id)
    select =
      "nickname stream_key email description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr";
  else
    select =
      "nickname description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr";

  User.findOne({ _id: id }, select)
    .populate({
      path: "followers",
      select: "follower",
      populate: { path: "follower", select: "nickname display_name url_profile_picture" },
    })
    .populate({
      path: "followed",
      select: "followee",
      populate: { path: "followee", select: "nickname display_name url_profile_picture" },
    })
    .then((data) => {
      if (data) res.status(200).json(data.toObject());
      else res.status(404).json({});
    })
    .catch((err) => {
      console.error(err);
      res.status(400).json({ error: err });
    });
};

// FIND ONE BY USERNAME
exports.findOneByUsername = (req, res) => {
  let username = req.params.username;
  select =
    "nickname description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr";

  User.findOne({ nickname: username }, select)
    .populate({
      path: "followers",
      select: "follower",
      populate: { path: "follower", select: "nickname display_name url_profile_picture" },
    })
    .populate({
      path: "followed",
      select: "followee",
      populate: { path: "followee", select: "nickname display_name url_profile_picture" },
    })
    .then((data) => {
      if (data) res.status(200).json(data);
      else res.status(404).json({});
    })
    .catch((err) => {
      console.error(err);
      res.status(400).json({ error: err });
    });
};

// FIND ONE BY LIKE USERNAME
exports.findByLikeUsername = (req, res) => {
  let username = req.params.username;
  User.find(
    { nickname: { $regex: ".*" + username + ".*" } },
    "nickname description url_profile_picture url_banner display_name stream_name stream_description tags instagram twitter youtube soundcloud facebook tumblr"
  )
    .limit(5)
    .then((data) => {
      if (data) res.status(200).json(data);
      else res.status(404).json({});
    })
    .catch((err) => {
      console.error(err);
      res.status(400).json({ error: err });
    });
};

// UPDATE
exports.update = (req, res) => {
  if (req.body.password) {
    const salt = bcrypt.genSaltSync(10);
    req.body.password = bcrypt.hashSync(req.body.password, salt);
  }
  if (req.user.id !== req.params.id) {
    res.status(403).send({message: "You are not authorized to do this."});
  } else {
    User.existingNickname(req.body.nickname).then((nicknameExist) => {
      if (req.user.nickname !== req.body.nickname && nicknameExist) {
        res.status(409).json({message: "User nickname already exists"});
      } else {
        User.findOneAndUpdate({_id: req.params.id}, {$set: req.body})
            .then((user) => {
              if (user) {
                User.findOne({_id: user._id}).then((newUser) => {
                  if (newUser) {
                    const token = jwt.sign(
                        {
                          id: newUser._id,
                          nickname: newUser.nickname,
                          url_profile_picture: newUser.url_profile_picture,
                        },
                        process.env.JWT_SECRET,
                        {algorithm: "HS512"},
                        {expiresIn: "24h"}
                    );
                    res.status(200).json({
                      newUser: newUser,
                      token: token,
                    });
                  } else res.status(500);
                });
              } else {
                res.status(404).json({error: "User not found"});
              }
            })
            .catch((err) => {
              console.error(err);
              res.status(400).json({error: err});
            });
      }

    })
  }
}

// REMOVE
exports.remove = (req, res) => {
  if (req.user.id !== req.params.id) {
    res.status(403).send({ message: "You are not authorized to do this." });
  } else {
    User.findOneAndDelete({ _id: req.params.id })
      .then((deleted_user) => {
        if (deleted_user) res.status(204).send();
        else res.status(404).json({ error: "User not found" });
      })
      .catch((err) => {
        console.error(err);
        res.status(400).json({ error: err });
      });
  }
};

// Generate a new stream key
exports.generateKeyStream = async (req, res) => {
  if (req.user.id !== req.body._id) {
    res.status(403).send({ message: "You are not authorized to do this." });
  } else {
    try {
      await User.findOneAndUpdate(
        { _id: req.body._id },
        {
          stream_key: User.generateShortId(),
        }
      );

      const newUser = await User.findOne({ _id: req.body._id });
      return res.json({
        stream_key: newUser.stream_key,
      });
    } catch (e) {
      console.error(e)
      res.status(500).json({ error: e.message });
    }
  }
};
