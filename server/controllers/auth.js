const User = require("../models/user.js");
const createToken = require("../lib/jwt").createToken;
const nodemailer = require('nodemailer');
const dotenv = require('dotenv');
const bcrypt = require("bcryptjs");

dotenv.config();

/*
    STMP details
*/
var smtpTransport = nodemailer.createTransport({
    service: "Gmail",
    auth: {
        user: process.env.GMAIL_USER,
        pass: process.env.GMAIL_PASS
    }
});
/*------------------SMTP Over-----------------------------*/

var exports = (module.exports = {});

// Authentification
exports.authenticate = (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    if (!email || !password)
        return res.status(400).send({ error: "Missing parameters" });

    User.findByCredentials(email, password)
        .then(user => {
            if (!user.verified) {
                return res.status(400).send({ error: "Account not verified !" });
            }
            const token = createToken(user);
            return res.status(200).send({ token: token });
        })
        .catch(err => {
            console.error(err)
            return res.status(401).send({ error: err });
        });
};

// Register
exports.register = async (req, res) => {

    // TODO: Contrôle de la donnée
    const email = req.body.email;
    const nickname = req.body.nickname;
    const password = req.body.password;

    // Creation of user
    const user = new User({
        email: email,
        nickname: nickname,
        password: password,
    });
    user.register().then(data => {
        let link = "http://" + req.get('host') + "/verify?id=" + user.verification_id;
        let mailOptions = {
            to: email,
            subject: "[TUNEL] Account verification",
            html: "Hello,<br> Please Click on the link to verify your tunel account.<br><a href=" + link + " target=\"_blank\">Click here to verify</a>"
        }
        try {
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                    res.end("error");
                } else {
                    res.status(201).send({
                        id: data._id,
                        email: data.email,
                        nickname: data.nickname,
                    });
                }
            });
        } catch (e) {
            console.log(e);
        }
    }).catch(err => {
        console.error(err)
        if (err == "InternalServerError")
            res.status(500).send({ error: "Unexpected server error" });

        res.status(400).send({ error: err });
    });
};

exports.newPassword = async (req, res) => {
    const email = req.body.email;
    const salt = bcrypt.genSaltSync(10);
    const clearPassword = User.generateShortId();
    const password = bcrypt.hashSync(clearPassword, salt);

    User.findOneAndUpdate({ email: email }, { password: password }).then((user) => {
        if (user) {
            let mailOptions = {
                to: email,
                subject: "[TUNEL] your new password",
                html: "Hello,<br> Your new password is <b>" + clearPassword + "</b>. Please log in Tunel and change your password as soon as possible."
            }
            smtpTransport.sendMail(mailOptions, function (error, response) {
                if (error) {
                    console.log(error);
                    res.end("error");
                } else {
                    res.status(200).send({ message: "Email sent !" });
                }
            });
        }
        else res.status(500).send({ error: "User not found" });
    }).catch(err => {
        console.error(err)
        if (err == "InternalServerError")
            res.status(500).send({ error: "Unexpected server error" });

        res.status(400).send({ error: err });
    });
};

// Venification
exports.verify = (req, res) => {
    const verification_id = req.query.id;
    User.findOneAndUpdate({ verification_id: verification_id }, { verified: true })
        .then((user) => {
            if (user) {
                User.findOne({ _id: user._id }).then((newUser) => {
                    if (newUser) {
                        res.writeHead(302, {
                            Location: process.env.REACT_APP_URL + 'success'
                        });
                        res.end();
                    }
                    else res.status(500);
                });
            } else {
                res.status(404).json({ error: "User not found" });
            }
        })
        .catch((err) => {
            console.error(err);
            res.status(400).json({ error: err });
        });
};
