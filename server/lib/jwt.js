const jwt = require("jsonwebtoken");

// Token creation
const createToken = (user = {}) => {
  return jwt.sign(
    { id: user._id, nickname: user.nickname, url_profile_picture: user.url_profile_picture },
    process.env.JWT_SECRET,
    { algorithm: "HS512" },
    { expiresIn: "24h" }
  );
};

// Token verification
const verifyToken = token => {
  return new Promise((resolve, reject) => {
    jwt.verify(token, process.env.JWT_SECRET, (err, decodedToken) => {
      if (err || !decodedToken) reject(err);
      resolve(decodedToken);
    });
  });
};

module.exports = {
  createToken,
  verifyToken
};
