const mongoose = require("mongoose");


if (process.env.NODE_ENV !== "production") {
    mongoose.connect(process.env.MONGODB_URI || "mongodb://" + (process.env.MONGO_URL ? process.env.MONGO_URL : "mongo:27017"), {
        user: process.env.MONGO_USER,
        pass: process.env.MONGO_PASSWORD,
        dbName: process.env.MONGODB_DBNAME,
        useNewUrlParser: true
    });
} else {
    mongoose.connect(process.env.MONGODB_URI || "mongodb://" + process.env.MONGO_USER + ":" + process.env.MONGO_PASSWORD + "@" + process.env.MONGO_URL + ":27017/tunel", { useNewUrlParser: true });
}
const db = mongoose.connection;

db.once("open", () => console.log("[MONGO] Connected to the database successfully"));
db.on("error", error => console.error("Error", error));

module.exports = mongoose.connection;
