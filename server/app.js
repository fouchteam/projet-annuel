const express = require("express");
const authRouter = require("./routes/auth");
const userRouter = require("./routes/user");
const followRouter = require("./routes/follow");
const streamRouter = require("./routes/streams");
const cors = require("cors");
const bodyparser = require("body-parser");
var cookieParser = require("cookie-parser");
const socketIo = require("socket.io");
const http = require("http");

// next to all imports
const node_media_server = require("./MediaServer/media_server");
const verifyAuthentication = require("./middleware/auth");

const app = express();

if (process.env.NODE_ENV === "production") {
  allowedOrigins = ['http://tunl.tv', 'http://www.tunl.tv', 'https://tunl.tv', 'https://www.tunl.tv'];
} else {
  allowedOrigins = ['http://localhost:3001', 'https://localhost:3001'];
}

var corsOption = {
  credentials: true,
  origin: function (origin, callback) {
    if (!origin) return callback(null, true); if (allowedOrigins.indexOf(origin) === -1) {
      var msg = 'The CORS policy for this site does not ' +
        'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    } return callback(null, true);
  }
};

app.use(cors(corsOption));
app.options("*", cors(corsOption));

app.use(bodyparser.json({ limit: "50mb" }));
app.use(cookieParser());

app.get("/status", (_, res) => {
  res.status(200).send({ message: "Connected" });
});

// --- Auth routes ---
app.use("/", authRouter);
app.use("/streams", streamRouter);

// --- Verify if connected ---
app.use(verifyAuthentication);

// User routes
app.use("/users", userRouter);

// Follow routes
app.use("/follow", followRouter);

// ------------------------------------------------ //
app.get("*", (req, res) => {
  res.status(404).send({ message: "Route not found" });
});

// Socket
const socket = express();
socket.use(cors(corsOption));
socket.options("*", cors(corsOption));
const socketServer = http.createServer(socket);

const io = require("socket.io")(socketServer, {
  handlePreflightRequest: (req, res) => {
    const headers = {
      "Access-Control-Allow-Headers": "Content-Type, Authorization",
      "Access-Control-Allow-Origin": req.headers.origin,
      "Access-Control-Allow-Credentials": true,
    };
    res.writeHead(200, headers);
    res.end();
  },
});

io.on("connection", (socket) => {
  var room = socket.handshake.query.room;
  socket.join(room);
  io.in(room).emit(
    "new_spectator",
    io.sockets.adapter.rooms[room] ? io.sockets.adapter.rooms[room].length : 0
  );
  socket.on("from_client", (query) => {
    socket.to(query.data.room).emit("from_server", query.data.message);
  });
  socket.on("disconnect", () => {
    io.in(room).emit(
      "spectator_disconnected",
      io.sockets.adapter.rooms[room] ? io.sockets.adapter.rooms[room].length : 0
    );
  });
});

io.set("origins", "*:*");

socketServer.listen(process.env.PORT_WS || 4000, () =>
  console.log(`[INFO] Socket Server listening on port ` + process.env.PORT_WS || 4000)
);

// and call run() method at the end
// file where we start our web server
node_media_server.run();

module.exports = app

// --- Listen routes ---
app.listen(process.env.PORT || 3000, "0.0.0.0", () =>
  console.log("[INFO] Server listening on port " + process.env.PORT || 3000)
);