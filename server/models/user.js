const validator = require("validator");
const mongoose = require("mongoose");
const db = require("../lib/db");
const bcrypt = require("bcryptjs");
const shortid = require("shortid");

// USER
const UserSchema = mongoose.Schema(
  {
    // Account
    nickname: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      validate: [validator.isEmail, "Invalid email"],
      unique: true,
    },

    // Informations
    display_name: String,
    stream_key: String,
    description: String,
    url_profile_picture: String,
    url_banner: String,

    // Social media
    instagram: String,
    twitter: String,
    youtube: String,
    soundcloud: String,
    facebook: String,
    tumblr: String,

    // Stream
    stream_name: String,
    tags: [Object],
    stream_description: String,

    // Utils
    verification_id: String,
    verified: Boolean,
    deleted: Boolean,
    created_at: Date,
  },
  { toObject: { virtuals: true, transform: true }, toJSON: { virtuals: true } }
);

// Virtual field followers
UserSchema.virtual("followers", {
  ref: "Follow",
  localField: "_id",
  foreignField: "followee",
  justOne: false,
  toObject: true,
});

// Virtual field followed
UserSchema.virtual("followed", {
  ref: "Follow",
  localField: "_id",
  foreignField: "follower",
  justOne: false,
});

// Manage to object
if (!UserSchema.options.toObject) UserSchema.options.toObject = {};
UserSchema.options.toObject.transform = function (doc, ret, options) {
  delete ret.id;
  return ret;
};

// Pre save hash password
UserSchema.pre("save", function (next) {
  if (this.password) {
    const salt = bcrypt.genSaltSync(10);
    this.password = bcrypt.hashSync(this.password, salt);
  }
  next();
});

// Register
UserSchema.methods.register = function () {
  return new Promise((resolve, reject) => {
    // Set attributes
    this.created_at = new Date();
    this.stream_key = User.generateShortId();
    this.verification_id = User.generateShortId();
    this.verified = false;

    // Missing parameters
    if (!this.email || !this.nickname || !this.password)
      reject("Missing parameters email, nickname or password");

    // MAIL EXISTS
    User.existingEmail(this.email)
      .then((mailExist) => {
        if (mailExist) {
          reject("User email already exists");
        } else {
          // NICKNAME EXISTS
          User.existingNickname(this.nickname).then((nicknameExist) => {
            if (nicknameExist) {
              reject("User nickname already exists");
            } else {
              // SAVE
              return this.save().then((user) => {
                resolve(user);
              });
            }
          });
        }
      })
      .catch((err) => {
        console.error(err);
        reject("A server error occurred during the process of registration");
      });
  });
};

// Find by email and password
UserSchema.statics.findByCredentials = function (email, password) {
  return new Promise((resolve, reject) => {
    User.findOne({ email })
      .then((user) => {
        if (!user) return reject("Invalid credentials");
        const isPasswordValid = bcrypt.compareSync(password, user.password);
        
        if (isPasswordValid) resolve(user);
        else reject("Invalid credentials");
      })
      .catch((err) => {
        reject(err);
      });
  });
};

// Check if email exists
UserSchema.statics.existingEmail = async function (email) {
  return new Promise((resolve, reject) => {
    User.findOne({ email: email })
      .then((user) => {
        if (user) return resolve(true);
        else return resolve(false);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

// Check if nickname exists
UserSchema.statics.existingNickname = async function (nickname) {
  return new Promise((resolve, reject) => {
    User.findOne({ nickname: nickname })
      .then((user) => {
        if (user) return resolve(true);
        else return resolve(false);
      })
      .catch((err) => {
        reject(err);
      });
  });
};

// Generate a unique id for boradcasting stream key and email verification
UserSchema.statics.generateShortId = () => {
  return shortid.generate();
};

// Store user model
const User = db.model("User", UserSchema);
module.exports = User;
