const db = require("../lib/db");
var mongoose = require("mongoose");

var FollowSchema = new mongoose.Schema(
  {
    follower: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    followee: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
  },
  {
    timestamps: { createdAt: "created_at", updatedAt: "updated_at" },
    toObject: { virtuals: true },
    toJSON: { virtuals: true },
  }
);

// Manage to object
if (!FollowSchema.options.toObject) FollowSchema.options.toObject = {};
FollowSchema.options.toObject.transform = function (doc, ret, options) {
  delete ret.id;
  return ret;
};

const Follow = db.model("Follow", FollowSchema);
module.exports = Follow;
