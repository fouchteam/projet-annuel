# Tunel

## Documentation

- [WIKI](https://gitlab.com/fouchteam/projet-annuel/wikis/home)

## Initialisation

Pour démarer l'application : 

```bash 
$ docker-compose up
```

- [React sur 127.0.0.1:3001](http://localhost:3001/)

- [API node sur 127.0.0.1:3000](http://localhost:3000/)

## Développement

Pour ajouter des dépendances : 

```bash
docker-compose run <server/app> yarn add <package>
```

## Tests

Avec tous les containeurs inactifs :

```bash
docker-compose run server yarn run test
```