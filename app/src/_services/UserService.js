import axios from "axios";
import { isString } from "formik";
import { urlConstants } from "../_constants/url.constants";
import { endpointConstants } from "../_constants/endpoint.constants";

/**
 * Login to the app
 *
 * @param {*} playload corresponds to the payload for connexion
 */
function login(playload) {
  return axios
    .post(
      urlConstants.API_BASE_URL + endpointConstants.LOGIN_ENDPOINT,
      playload
    )
    .then(function (response) {
      return response;
    })
    .catch(function (error) {
      return error.response;
    });
}

/**
 * Register to the app
 *
 * @param {*} payload corresponds to the payload to register
 */
export function register(payload) {
  return axios
    .post(
      urlConstants.API_BASE_URL + endpointConstants.REGISTER_ENDPOINT,
      payload
    )
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      return error.response;
    });
}

/**
 * Change password
 *
 * @param {*} payload corresponds to the payload to register
 */
export function newPassword(payload) {
  return axios
    .post(
      urlConstants.API_BASE_URL + endpointConstants.PASSWORD_ENDPOINT,
      payload
    )
    .then((response) => {
      return response;
    })
    .catch(function (error) {
      return error.response;
    });
}

/**
 * Logout to the app
 */
export function logout() {
  localStorage.removeItem("token");
}

/**
 * Method to get one user by id
 *
 * @param {*} id corresponds to an mongodb ObjectId
 */
export async function getAllUsers() {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    const res = await axios.get(
      urlConstants.API_BASE_URL + endpointConstants.USER_ENDPOINT,
      config
    );
    return res;
  } catch (e) {
    throw handler(e);
  }
}


/**
 * Method to get one user by id
 *
 * @param {*} id corresponds to an mongodb ObjectId
 */
export async function getOneUser(id) {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    const res = await axios.get(
      urlConstants.API_BASE_URL + endpointConstants.USER_ENDPOINT + "/" + id,
      config
    );
    return res;
  } catch (e) {
    throw handler(e);
  }
}

/**
 * Method to get a User from a nickname
 *
 * @param {String} username corresponds to a username of an account
 */
export async function getUserByUsername(username) {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    const res = await axios.get(
      urlConstants.API_BASE_URL +
      endpointConstants.USER_ENDPOINT +
      "/username/" +
      username,
      config
    );
    return res;
  } catch (e) {
    throw e;
  }
}

/**
 * Method to get all User like the passed username
 *
 * @param {String} username corresponds to a username of an account
 */
export async function getUsersByUsernameLike(username) {
  const TOKEN = localStorage.getItem("token");
  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };
  try {
    const res = await axios.get(
      urlConstants.API_BASE_URL +
      endpointConstants.USER_ENDPOINT +
      "/search/username/" +
      username,
      config
    );
    return res;
  } catch (e) {
    throw handler(e);
  }
}

/**
 * Method to update an user
 *
 * @param {*} id corresponds to a MongoDB ObjectID
 * @param {*} data corresponds to the payload
 */
export async function updateUser(id, data) {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  const payload = {
    nickname: data.nickname,
    tags: data.tags,
    email: data.email,
    description: data.description,
    display_name: data.display_name,
    url_profile_picture: data.url_profile_picture,
    url_banner: data.url_banner,
    stream_name: data.stream_name,
    stream_description: data.stream_description,
    instagram: data.instagram,
    twitter: data.twitter,
    youtube: data.youtube,
    soundcloud: data.soundcloud,
    facebook: data.facebook,
    tumblr: data.tumblr
  };

  try {
    return await axios.put(
      urlConstants.API_BASE_URL + endpointConstants.USER_ENDPOINT + "/" + id,
      payload,
      config
    );
  } catch (e) {
    throw handler(e);
  }
}

/**
 * Method to update user password
 *
 * @param {*} id corresponds to a MongoDB ObjectID
 * @param {*} data corresponds to the payload
 */
export async function updateUserPassword(id, data) {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  const payload = {
    password: data.password
  };

  try {
    return await axios.put(
      urlConstants.API_BASE_URL + endpointConstants.USER_ENDPOINT + "/" + id,
      payload,
      config
    );
  } catch (e) {
    throw handler(e);
  }
}

/**
 * Method to regenerate a stream key for one user
 *
 * @param {*} id corresponds to the user mongoDB ObjectId
 */
export const genStreamKey = async (id) => {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  return axios.post(
    urlConstants.API_BASE_URL + "/users" + endpointConstants.GEN_STREAM_KEY,
    {
      _id: id,
    },
    config
  );
};

/**
 * Method to follow an user
 *
 * @param {*} id corresponds to a user mongoDB ObjectID
 */
export const followAnUser = async (id) => {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    return axios.post(urlConstants.API_BASE_URL + "/follow/" + id, {}, config);
  } catch (e) {
    throw handler(e);
  }
};

/**
 * Method to get followers of one user
 *
 * @param {*} id corresponds to a user mongoDB ObjectID
 */
export const getFollowers = async (id) => {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    return axios.get(
      urlConstants.API_BASE_URL + "/follow/followers/" + id,
      config
    );
  } catch (e) {
    throw handler(e);
  }
};

/**
 * Method to get followed for one user
 *
 * @param {*} id corresponds to a user mongoDB ObjectID
 */
export const getFollowed = async (id) => {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    return axios.get(
      urlConstants.API_BASE_URL + "/follow/followed/" + id,
      config
    );
  } catch (e) {
    throw handler(e);
  }
};

/**
 * Method to unfollow an user
 *
 * @param {*} id corresponds to a user mongoDB ObjectID
 */
export const unfollowAnUser = async (id) => {
  const TOKEN = localStorage.getItem("token");

  const config = {
    headers: { Authorization: `Bearer ${TOKEN}` },
  };

  try {
    return axios.delete(urlConstants.API_BASE_URL + "/follow/" + id, config);
  } catch (e) {
    throw handler(e);
  }
};

/**
 * Method to handle errors
 *
 * @param {*} err corresponds to an http error
 */
export function handler(err) {
  let error = err;
  if (err.response && err.response.data.hasOwnProperty("message")) {
    error = err.response.data;
  } else if (err.response && isString(err.response.data)) {
    error.message = err.response.data;
  } else if (!err.hasOwnProperty("message")) {
    error = err.toJSON();
  }

  return new Error(error.message);
}

export default {
  login,
  register,
  newPassword,
  logout,
  getOneUser,
  genStreamKey,
  updateUser,
};
