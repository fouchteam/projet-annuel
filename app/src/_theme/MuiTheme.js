import { createMuiTheme } from "@material-ui/core";


const themeObject = {
  palette: {
    type: "dark",
    primary: {
      main: "#0064c7",
      dark: "#004a94",
    },
    secondary: {
      main: "#6f0094",
      dark: "#6f0094",
    },
    background: {
      paper: "#222222",
      default: "#131313",
    },
    border: {
      main: '#4a4a4a'
    },
    blue: {
      primary: '#11acff'
    }
  },
  typography: {
    fontFamilly: [
      '"Nunito Sans"',
      "Roboto",
      "Helvetica",
      "Arial",
      "sans-serif",
    ].join(","),
  },
};

export const muiTheme = createMuiTheme(themeObject);