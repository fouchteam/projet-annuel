// Action Types
export const LOGGED_IN = `auth/LOGGED_IN`;
export const LOGGED_OUT = `auth/LOGGED_OUT`;
export const IS_REGISTERED = `auth/IS_REGISTERED`;
export const IS_LOGGED = `auth/IS_LOGGED`;
export const IS_TOKEN_UPDATED = `auth/IS_TOKEN_UPDATED`;

export const initialState = {
  // Flag
  isRegistered: false,
  isLogged: false,
  // Token
  token: "",
};

// REDUCER
const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGGED_IN: {
      return {
        ...state,
        isLogged: true,
        token: action.token,
      };
    }

    case LOGGED_OUT: {
      return {
        token: "",
        isLogged: false,
      };
    }

    case IS_REGISTERED: {
      return {
        isRegistered: action.isRegistered,
      };
    }

    case IS_LOGGED: {
      return {
        ...state,
        isLogged: action.isLogged,
        token: action.token,
      };
    }

    case IS_TOKEN_UPDATED: {
      let { token } = action;
      return { ...state, token };
    }

    default:
      return state;
  }
};

export default authReducer;
