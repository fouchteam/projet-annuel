import { initialValues } from "../Context/FollowContext";

//Action Types
export const GOT_FOLLOWS = `follow/GOT_FOLLOWS`;
export const GOT_FOLLOWERS = `follow/GOT_FOLLOWERS`;


//REDUCER
const followReducer = (state = initialValues, action) => {
  switch (action.type) {
    case GOT_FOLLOWS: {
      let { list } = action;
      return { ...state, followsList: list };
    }

    case GOT_FOLLOWERS: {
      let { list } = action;
      return { ...state, followersList: list };
    }

    default:
      return state;
  }
};

export default followReducer;
