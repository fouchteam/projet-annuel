import { initialValues } from "../Context/AccountContext";

//Action Types
export const GOT_ACCOUNT = `account/GOT_ACCOUNT`;
export const PUT_ACCOUNT = `account/PUT_ACCOUNT`;
export const UPDATED_STREAM_KEY = `account/UPDATED_STREAM_KEY`;
export const ACCOUNT_LOADING_ENABLE = `account/LOADING_ENABLE`;
export const ACCOUNT_LOADING_DISABLE = `account/LOADING_DISABLE`;


//REDUCER
const accountReducer = (state = initialValues, action) => {
  switch (action.type) {
    case GOT_ACCOUNT: {
      let { user } = action;
      return { ...state, currentUser: user };
    }

    case PUT_ACCOUNT: {
      let { user } = action;
      return { ...state, currentUser: { ...state.currentUser, ...user } };
    }

    case UPDATED_STREAM_KEY: {
      let { stream_key } = action;
      return { ...state, currentUser: { ...state.currentUser, stream_key: stream_key } };
    }

    case ACCOUNT_LOADING_ENABLE: {
      return { ...state, isLoading: true };
    }

    case ACCOUNT_LOADING_DISABLE: {
      return { ...state, isLoading: false };
    }

    default:
      return state;
  }
};

export default accountReducer;
