import { initialValues } from "../Context/UserContext";

//Action Types
export const GOT_USER = `user/GOT_USER`;
export const GOT_USERS = `user/GOT_USERS`;
export const GOT_USERS_HOMEPAGE = `user/GOT_USERS_HOMEPAGE`;
export const USER_LOADING_ENABLE = `user/LOADING_ENABLE`;
export const USER_LOADING_DISABLE = `user/LOADING_DISABLE`;

//REDUCER
const userReducer = (state = initialValues, action) => {
  switch (action.type) {
    case GOT_USER: {
      let { user } = action;
      return { ...state, user: user };
    }

    case GOT_USERS: {
      let { users } = action;
      return { ...state, userList: users };
    }

    case GOT_USERS_HOMEPAGE: {
      let { users } = action;
      return { ...state, usersHomepage: users }
    }

    case USER_LOADING_ENABLE: {
      return { ...state, isLoading: true };
    }

    case USER_LOADING_DISABLE: {
      return { ...state, isLoading: false };
    }

    default:
      return state;
  }
};

export default userReducer;
