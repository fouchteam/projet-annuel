import { createContext } from "react";

export const initialValues = {
  usersHomepage: [],
  userList: [],
  user: null,
  retrieveUserListByLikeUsername: async () => {},
  retrieveUserByNickname: async () => {},
  retrieveUserHomepage: async () => {}
};

export const UserContext = createContext(initialValues);
