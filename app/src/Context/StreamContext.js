import { createContext } from "react";

export const initialValues = {
  liveStreams: [],
  currentStream: {},
  getLiveStreams: () => {},
  setCurrentStream: () => {},
  getUsersStream: () => {},
};

export const StreamContext = createContext(initialValues);
