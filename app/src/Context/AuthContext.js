import {createContext} from "react"

export const initialValues = {
    isLogged: false,
    isRegistered: false,
    token: "",

    login: () => {},
    register: () => {},
    logout: () => {},
    newPassword: () => {},
    setIsRegistered: () => {},
    setIsLogged:() => {},
    setIsTokenUpdated:() => {}
};

export const AuthContext = createContext(initialValues);