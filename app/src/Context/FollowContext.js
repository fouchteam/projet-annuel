import { createContext } from "react";

export const initialValues = {
  followsList: null,
  followersList: null,
  followOneUser: async () => {},
  unfollowOneUser: async () => {},
  getFollowedOfCurrentUser: async () => {},
};

export const FollowContext = createContext(initialValues);
