import { createContext } from "react"

export const initialValues = {
    currentUser: null,
    isLoading: false,
    retrieveCurrentUser: async () => { },
    updateCurrentUser: async () => { },
    generateStreamKey: async () => { },
    updateCurrentUserPassword: async () => { }
};

export const AccountContext = createContext(initialValues);