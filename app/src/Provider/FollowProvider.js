import React, { useMemo, useReducer, useContext } from "react";
import { withAlert } from "./AlertProvider";

import { FollowContext, initialValues } from "../Context/FollowContext";
import followReducer, {
  GOT_FOLLOWS
} from "../Reducer/FollowReducer";
import {
  followAnUser,
  getFollowed,
  unfollowAnUser,
} from "../_services/UserService";
import { tokenConstants } from "../_constants/token.constants";
import jwt_decode from "jwt-decode";

const FollowProvider = withAlert(({ children, error }) => {
  const [state, dispatch] = useReducer(followReducer, initialValues || {});

  /**
   * Follow a user
   *
   * @param {*} id corresponds to a mongoDb ObjectId
   */
  const followOneUser = async (id) => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const response = await followAnUser(id);
        if (response.status === 200) {
          const response = await getFollowed(
            jwt_decode(localStorage.getItem(tokenConstants.TOKEN_KEY)).id
          );
          const list = response.data;

          dispatch({ type: GOT_FOLLOWS, list: list });
        }
      }
    } catch (e) {
      error("Unable follow");
    }
  };

  /**
   *  Get followed people of current user
   */
  const getFollowedOfCurrentUser = async () => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const response = await getFollowed(
          jwt_decode(localStorage.getItem(tokenConstants.TOKEN_KEY)).id
        );
        const list = response.data;

        dispatch({ type: GOT_FOLLOWS, list: list });
      }
    } catch (e) {
      error("Unable to retrieve follows");
    }
  };

  /**
   * Unfollow an user
   *
   * @param {*} id corresponds to a mongoDb ObjectId
   */
  const unfollowOneUser = async (id) => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const response = await unfollowAnUser(id);
        if (response.status === 204) {
          const response = await getFollowed(
            jwt_decode(localStorage.getItem(tokenConstants.TOKEN_KEY)).id
          );
          const list = response.data;

          dispatch({ type: GOT_FOLLOWS, list: list });
        }
      }
    } catch (e) {
      error("Unable to follow");
    }
  };

  const value = useMemo(() => {
    return {
      state,
      followOneUser,
      unfollowOneUser,
      getFollowedOfCurrentUser,
    };
  }, [state]);

  return (
    <FollowContext.Provider value={value}>{children}</FollowContext.Provider>
  );
});

const useFollow = () => useContext(FollowContext);
export { useContext, useFollow, FollowProvider };
