import React, {useState, useEffect} from "react";
import {initialValues, StreamContext} from "../Context/StreamContext";
import axios from "../_helper/axios/config";
import {withAlert} from "./AlertProvider";

export const StreamProvider = withAlert(({error, children}) => {
    const [state, setState] = useState({
        ...initialValues,
        getLiveStreams: () => {
            axios.get('/streams/')
                .then(res => {
                    const live = res.data;
                    if (typeof (live) !== 'undefined') {
                        setState(prevState => {
                            return {...prevState, liveStreams: live}
                        });
                    }
                })
                .catch(() => {
                    error("An error occured while fetching streams");
                });
        },
        setCurrentStream: (stream) => {
            setState(prevState => ({
                ...prevState,
                currentStream: stream
            }))
        },
        getUsersStream: (username) => {
            return axios.get(`/streams/${username}`)
                .then(resp => {
                    state.setCurrentStream(resp.data);
                });
        },
    });

    return <StreamContext.Provider value={state}>
        {children}
    </StreamContext.Provider>
});
