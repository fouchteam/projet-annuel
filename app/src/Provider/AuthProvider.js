import React, { useEffect, useMemo, useReducer } from "react";
import { AuthContext } from "../Context/AuthContext";
import UserService from "../_services/UserService";
import { withAlert } from "./AlertProvider";
import AuthReducer, {
  initialState,
  IS_LOGGED,
  IS_REGISTERED,
  LOGGED_IN,
  LOGGED_OUT,
  IS_TOKEN_UPDATED,
} from "../Reducer/AuthReducer";

// Provider
export const AuthProvider = withAlert(({ children, error, success }) => {
  const [state, dispatch] = useReducer(AuthReducer, initialState || {});

  /**
   * Log a user
   *
   * @param {*} playload corresponds to the needed payload for the request
   */
  const login = (playload) => {
    return UserService.login(playload).then(
      (response) => {
        if (response) return response;
      },
      (error) => {
        throw new Error(error.toString());
      }
    );
  };

  /**
   * Deconnect a user
   *
   */
  const logout = () => {
    localStorage.removeItem("token");
    dispatch({ type: LOGGED_OUT });
  };

  /**
   * Register a user
   *
   * @param {*} playload corresponds to the payload to register
   */
  const register = (playload) => {
    return UserService.register(playload).then(
      (response) => {
        if (response) return response;
      },
      (error) => {
        throw new Error(error.toString());
      }
    );
  };

  /**
   * Set new password
   *
   * @param {*} payload corresponds to a payload
   */
  const newPassword = (payload) => {
    return UserService.newPassword(payload).then(
      (response) => {
        if (response) return response;
      },
      (error) => {
        throw new Error(error.toString());
      }
    );
  };

  /**
   * Set registered
   *
   * @param {*} value corresponds to the value of registering state
   */
  const setIsRegistered = (value) => {
    dispatch({ type: IS_REGISTERED, isRegistered: value });
  };

  /**
   * Set logged
   *
   * @param {*} value corresponds to the value of loggin state
   */
  const setIsLogged = (value) => {
    dispatch({
      type: IS_LOGGED,
      isLogged: value,
      token: localStorage.getItem("token"),
    });
  };

  /**
   * Token updated
   */
  const setIsTokenUpdated = () => {
    dispatch({ type: IS_TOKEN_UPDATED, token: localStorage.getItem("token") });
  };

  /**
   * Connexion check
   */
  useEffect(() => {
    if (localStorage.getItem("token")) {
      dispatch({
        type: LOGGED_IN,
        token: localStorage.getItem("token"),
      });
    } else {
      dispatch({ type: LOGGED_OUT });
    }
  }, []);

  /**
   * Use memo
   */
  const value = useMemo(() => {
    return {
      state,
      login,
      logout,
      register,
      setIsRegistered,
      setIsLogged,
      setIsTokenUpdated,
      newPassword,
    };
  }, [state]);

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
});
