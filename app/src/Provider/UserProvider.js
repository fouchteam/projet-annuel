import React, { useMemo, useReducer, useContext } from "react";
import {
  getUserByUsername,
  getUsersByUsernameLike,
  getAllUsers,
} from "../_services/UserService";
import { withAlert } from "./AlertProvider";
import { UserContext, initialValues } from "../Context/UserContext";
import userReducer, { GOT_USER, GOT_USERS, GOT_USERS_HOMEPAGE } from "../Reducer/UserReducer";

const UserProvider = withAlert(({ children, error }) => {
  const [state, dispatch] = useReducer(userReducer, initialValues || {});

  /**
   * Get users for homepage
   *
   */
  const retrieveUserHomepage = async () => {
    try {
      const response = await getAllUsers();
      const users = response.data;

      let someUsers = [];

      if (users.length > 4) {
        someUsers.push(users[Math.floor(Math.random() * users.length)]);
        someUsers.push(users[Math.floor(Math.random() * users.length)]);
        someUsers.push(users[Math.floor(Math.random() * users.length)]);
        someUsers.push(users[Math.floor(Math.random() * users.length)]);
      }


      dispatch({ type: GOT_USERS_HOMEPAGE, users: someUsers });
    } catch (e) {
      error("Unable to retrieve homepage users");
    }
  };

  /**
   * Get user by username
   *
   * @param {*} username corresponds to a string
   */
  const retrieveUserByNickname = async (username) => {
    try {
      const response = await getUserByUsername(username);
      const user = response.data;

      dispatch({ type: GOT_USER, user: user });
      return true;
    } catch (e) {
      if (e.response.status === 404) return false;
      error("Unable to retrieve user information");
    }
  };

  /**
   * Get user by nickname like
   *
   * @param {*} username corresponds to a string
   */
  const retrieveUsersByNicknameLike = async (username) => {
    try {
      const response = await getUsersByUsernameLike(username);
      const users = response.data;
      dispatch({ type: GOT_USERS, users: users });
    } catch (error) {
      error("Unable to retrieve user information");
    }
  };

  /**
   * Memo
   *
   */
  const value = useMemo(() => {
    return {
      state,
      retrieveUserByNickname,
      retrieveUsersByNicknameLike,
      retrieveUserHomepage
    };
  }, [state]);

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
});

const useUser = () => useContext(UserContext);
export { useContext, useUser, UserProvider };
