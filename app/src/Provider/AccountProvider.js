import React, { useMemo, useReducer, useContext, useEffect } from "react";
import {
  getOneUser,
  updateUser,
  genStreamKey,
  updateUserPassword,
} from "../_services/UserService";
import * as jwt_decode from "jwt-decode";
import { withAlert } from "./AlertProvider";

import { AccountContext, initialValues } from "../Context/AccountContext";
import accountReducer, {
  GOT_ACCOUNT,
  PUT_ACCOUNT,
  UPDATED_STREAM_KEY,
} from "../Reducer/AccountReducer";
import { tokenConstants } from "../_constants/token.constants";
import { AuthContext } from "../Context/AuthContext";

const AccountProvider = withAlert(({ children, error, success }) => {
  const [state, dispatch] = useReducer(accountReducer, initialValues || {});
  const authContext = useContext(AuthContext);

  /**
   * Recupération des données de l'utilisateur actuellement connecté
   *
   */
  const retrieveCurrentUser = async () => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const userId = jwt_decode(token).id;
        const response = await getOneUser(userId);
        const user = response.data;

        dispatch({ type: GOT_ACCOUNT, user: user });
      }
    } catch (e) {
      error("Unable to retrieve your personnal information");
    }
  };

  /**
   * Update data for the current user
   *
   * @param {*} user corresponds to the payload
   */
  const updateCurrentUser = async (user) => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const userId = jwt_decode(token).id;
        const response = await updateUser(userId, user);
        const new_user = response.data.newUser;

        await localStorage.setItem("token", response.data.token);
        dispatch({ type: PUT_ACCOUNT, user: new_user });

        success("Account updated");
      }
    } catch (e) {
      error(e.message);
    }
  };

  /**
   * Update current user password
   *
   * @param {*} password corresponds to a password
   */
  const updateCurrentUserPassword = async (password) => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const userId = jwt_decode(token).id;
        await updateUserPassword(userId, password);

        success("Password updated");
      }
    } catch (e) {
      error("An error occured while updating password");
    }
  };

  /**
   * Generate a new stream key for the current user
   */
  const generateStreamKey = async () => {
    try {
      let token = localStorage.getItem(tokenConstants.TOKEN_KEY);
      if (!token) {
        error("Please login");
      } else {
        const userId = jwt_decode(token).id;
        const { data } = await genStreamKey(userId);
        const { stream_key } = data;
        dispatch({ type: UPDATED_STREAM_KEY, stream_key: stream_key });

        success("Stream key reloaded");
      }
    } catch (e) {
      error("An error occured while generating the stream key");
    }
  };

  /**
   * Récupération des infos du compte connecté
   *
   */
  useEffect(() => {
    if (authContext.state.isLogged) {
      retrieveCurrentUser();
    }
  }, [authContext.state]);

  /**
   * Use memo
   */
  const value = useMemo(() => {
    return {
      state,
      retrieveCurrentUser,
      updateCurrentUser,
      updateCurrentUserPassword,
      generateStreamKey,
    };
  }, [state]);

  return (
    <AccountContext.Provider value={value}>{children}</AccountContext.Provider>
  );
});

// Use Account
const useAccount = () => useContext(AccountContext);
export { useContext, useAccount, AccountProvider };
