import React from "react";
import { Route, Router } from "react-router-dom";
import Homepage from "./Homepage/Homepage";
import Footer from "./Footer/Footer";
import Login from "./Connection/Login";
import Success from "./Connection/Success";
import Register from "./Connection/Register";
import { customHistory } from "../_helper/history";
import Header from "../Components/Header/Header";
import NotFound from "./Lib/NotFound";
import Account from "./Account/Account";
import { WithRouter } from "./Lib/WithRouter";
import { StreamProvider } from "../Provider/StreamProvider";
import { Player } from "./Lives/Player";
import RequireAuth from "./../_middleware/requireAuth";
import { FollowProvider } from "../Provider/FollowProvider";
import Follow from "./Follow/Follow";
import Password from "./Connection/Password";
import ErrorBoundary from "./Error/ErrorBoundary";
import Search from "./Search/Search";

export const Root = () => {
  return (
    <Router history={customHistory}>
      <div>
        <Header />
        <ErrorBoundary>
          <StreamProvider>
            <FollowProvider>
              <WithRouter>
                <Route exact path="/" component={Homepage} />
                <Route exact path="/search" component={Search} />
                <Route exact path="/login" component={Login} />
                <Route exact path="/register" component={Register} />
                <Route exact path="/password" component={Password} />
                <Route exact path="/success" component={Success} />
                <Route exact path="/account" component={RequireAuth(Account)} />
                <Route exact path="/follower/:username" component={Follow} />
                <Route
                  exact
                  path="/stream/:username"
                  render={(props) => <Player {...props} />}
                />
                <Route component={NotFound} />
              </WithRouter>
            </FollowProvider>
          </StreamProvider>
        </ErrorBoundary>
        <Footer />
      </div>
    </Router>
  );
};
