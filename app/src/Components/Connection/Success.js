import React from "react";
import { Container, Typography } from "@material-ui/core";
import SuccesImage from "../../assets/img/success-goal.svg";
import { Link } from "react-router-dom";

const Success = () => {
  return (
    <Container maxWidth="lg" style={{ marginTop: 140, textAlign: "center" }}>
        <img src={SuccesImage} width="250" alt="success"></img>
        <Typography variant="h5" style={{marginTop: 50}}>Success ! Your account has been verified.</Typography>
        <Typography style={{marginTop: 50, display: 'inline-flex'}}>
            Click&nbsp; 
            <Link to={"/login"} style={{ textDecoration: "none"}}><Typography color="textSecondary">Here</Typography></Link>
            &nbsp;to login.
        </Typography>
    </Container>
  );
};

export default Success;
