import React, {useContext, useEffect, useState} from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { Link } from "react-router-dom";
import Snackbar from "@material-ui/core/Snackbar";
import SnackbarContent from "@material-ui/core/SnackbarContent";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import { Redirect } from "react-router";
import { AuthContext } from "../../Context/AuthContext";
import { customHistory } from "../../_helper/history";
import { endpointConstants } from "../../_constants/endpoint.constants";
import {StaticAlert} from "../Lib/StaticAlert";
import CircularProgress from "@material-ui/core/CircularProgress";
import {green} from "@material-ui/core/colors";
import EmailImage from "../../assets/img/email.svg";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(10),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  success: {
    backgroundColor: theme.palette.success.main,
  },
  error: {
    backgroundColor: theme.palette.error.main,
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  },
  email : {
    marginTop: 80,
    marginBottom: 60
  }
}));

const Register = () => {
  const [open, setOpen] = React.useState(false);
  const [openSuccess, setOpenSuccess] = React.useState(false);
  const [errorMessage, setErrorMessage] = React.useState("");
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [nickname, setNickName] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const authContext = useContext(AuthContext);

  const initialState = {
    email: "",
    password: "",
    nickname: "",
  };

  const handleClose = (reason) => {
    if (reason === "clickaway") {
      return;
    }
    authContext.setIsRegistered(false);
    setOpen(false);
  };

  const clearState = () => {
    setEmail(initialState.email);
    setPassword(initialState.password);
    setNickName(initialState.nickname);
    setLoading(false);
  };

  function validateForm() {
    return email.length > 0 && password.length > 0 && nickname.length > 0;
  }


  const handleSubmit = (event) => {
    event.preventDefault();
    setLoading(true);
    authContext
      .register({
        email: email,
        password: password,
        nickname: nickname,
      })
      .then((response) => {
        if (response.data.error) {
          setErrorMessage(response.data.error);
          setOpen(true);
          return;
        }
        if (response) {
          authContext.setIsRegistered(true);
          setOpenSuccess(true)
        }
      })
        .finally(clearState);
  };


  return (
    <Container component="main" maxWidth="xs">
      {
        openSuccess ?
            <div className={classes.paper}>
              <img src={EmailImage} width="250" alt="email" className={classes.email}></img>
              <StaticAlert severity={"success"}>
                <Typography variant={"body1"}>
                  You have been registered ! Please go to your inbox to activate your account.
                </Typography>
              </StaticAlert>
            </div>
              :
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form onSubmit={handleSubmit} className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="nickname"
                label="Account Name"
                name="nickname"
                autoComplete="nickname"
                value={nickname}
                onChange={(e) => setNickName(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Grid>
          </Grid>
          <div className={classes.wrapper}>
            <Button
                type="submit"
                variant="contained"
                fullWidth
                color="primary"
                className={classes.submit}
                disabled={loading}
            >
              Sign Up
            </Button>
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
          </div>
          <Snackbar
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "left",
            }}
            open={open}
            autoHideDuration={4000}
            onClose={handleClose}
            className={classes.error}
          >
            <SnackbarContent
              className={classes.error}
              message={errorMessage}
              action={[
                <IconButton
                  key="close"
                  aria-label="close"
                  color="inherit"
                  className={classes.close}
                  onClick={handleClose}
                >
                  <CloseIcon />
                </IconButton>,
              ]}
            />
          </Snackbar>
          <Grid container justify="flex-end">
            <Grid item>
              <Link
                to={"/login"}
                variant="body2"
                style={{ textDecoration: "none" }}
              >
                <Typography color="textSecondary">
                  Already have an account?
                </Typography>
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      }
    </Container>
  );
};

export default Register;
