import React, {useState, useEffect} from "react";
import videojs from "video.js";
import "video.js/dist/video-js.css"

import "./css/style.css";

// import WaveSurfer from 'wavesurfer.js';
// import 'videojs-wavesurfer/dist/css/videojs.wavesurfer.css';
// import Wavesurfer from 'videojs-wavesurfer/dist/videojs.wavesurfer.js';

export const VideoPlayer = (props) => {
    const [player, setPlayer] = useState(null);
    const [videoNode, setVideoNode] = useState(null);

    useEffect(() => {
        if (videoNode) {
            const plugins = {
                plugins: {
                    // wavesurfer: {
                    //     src: props.sources[0].src,
                    //     msDisplayMax: 10,
                    //     debug: true,
                    //     waveColor: 'grey',
                    //     progressColor: 'black',
                    //     cursorColor: 'black',
                    //     hideScrollbar: true
                    // }
                }
            };
            const options = videojs.mergeOptions(props, plugins);
            setPlayer(videojs(videoNode, options));
        }
    }, [videoNode, props]);

    useEffect(() =>
        () => {
            if (player) {
                player.dispose();
            }
        }, []);

    return (
        <div>
            <div data-vjs-player>
                <video ref={node => setVideoNode(node)} className="video-js vjs-big-play-centered "/>
            </div>
        </div>
    )
};