import React from "react";
import { Route, Switch } from "react-router-dom";
import NotFound from "./NotFound";

export const WithRouter = ({ children }) => {
  return (
    <Switch>
      {children}
      <Route path="*">
        <NotFound />
      </Route>
    </Switch>
  );
};
