import React from "react";
import { Container, Typography } from "@material-ui/core";
import NotFoundImage from "../../assets/img/happy_music.svg";

const NotFound = () => {
  return (
    <Container maxWidth="lg" style={{ marginTop: 100, textAlign: "center" }}>
      <img src={NotFoundImage} width="250" alt="not found"></img>
      <Typography variant="h5" style={{marginTop: 50}}>404 - Page not found</Typography>
    </Container>
  );
};

export default NotFound;
