import React from "react";
import { Container, Typography } from "@material-ui/core";
import NotFoundImage from "../../assets/img/user_not_found.svg";

const NotFoundUser = () => {
  return (
    <Container maxWidth="lg" style={{ marginTop: 100, textAlign: "center" }}>
      <img src={NotFoundImage} width="250" alt="not found"></img>
      <Typography variant="h5" style={{ marginTop: 50 }}>
        User not found
      </Typography>
    </Container>
  );
};

export default NotFoundUser;
