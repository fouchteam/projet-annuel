import PropTypes from 'prop-types'
import React, { useMemo } from "react";
import { makeStyles } from "@material-ui/core";
import { Cached } from "@material-ui/icons";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { DefaultClasses } from "../../../_helper/DefaultClasses";
import Typography from "@material-ui/core/Typography/Typography";
import Box from "@material-ui/core/Box/Box";
import { urlConstants } from '../../../_constants/url.constants';

const useStyle = makeStyles(theme => ({
    ...DefaultClasses(theme),
    root: {
        flexGrow: 1,
    },
    button: {
        marginTop: 10
    }
}));

export const StreamKeyGen = ({ streamKey, regenStreamKey }) => {
    const classes = useStyle();
    return useMemo(() => (
        <div className={classes.root}>
            <Box mb={5}>
                <Typography variant="h6">Stream key</Typography>
                <Typography variant="caption">This key is private. Don't share it to others
                        !</Typography>
            </Box>
            <div className={classes.columnCenter}>
                <TextField
                    label="Private stream key"
                    variant="outlined"
                    disabled
                    InputLabelProps={{
                        shrink: true,
                    }}
                    value={streamKey} />
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<Cached />}
                    onClick={regenStreamKey}>
                    Re-generate
                </Button>
            </div>
            <Box mb={5} mt={5}>
                <Typography variant="subtitle1">URL for the RTMP stream</Typography>
                <Typography variant="button">{urlConstants.RTMP_SERVER_URL}</Typography>
            </Box>
        </div>
    ), [streamKey]
    );
};

StreamKeyGen.propTypes = {
    regenStreamKey: PropTypes.func.isRequired,
    streamKey: PropTypes.string.isRequired
};
