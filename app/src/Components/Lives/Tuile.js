import PropTypes from "prop-types";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { Spectator } from "./Stream/Spectator";
import { Category } from "./Stream/Category";
import { grey } from "@material-ui/core/colors";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import CardImg from "../../assets/img/vinyl.jpg";
import { Link } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
  root: {
    maxWidth: 320,
    margin: 10,
  },
  card: {
    maxWidth: 320,
  },
  details: {
    maxWidth: 320,
    paddingTop: 5,
    paddingLeft: 5,
    paddingRight: 5,
  },
  cover: {
    padding: 3,
    width: 320,
    height: 180,
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}));

export function Tuile({
  stream = {
    _id: 0,
  },
  title = "Title",
}) {
  const classes = useStyles();

  function TuileImage() {
    let tuileImage = `${CardImg}`;
    if (stream.thumbnail !== undefined)
      tuileImage = `data:image/png;base64,${stream.thumbnail}`;

    return tuileImage;
  }

  return (
    <div className={classes.root}>
      <Link
        href={"/stream/" + stream.nickname}
        style={{ textDecoration: "none", color: "inherit" }}
      >
        <Card className={classes.card}>
          <CardMedia className={classes.cover} image={TuileImage()}>
          </CardMedia>
        </Card>
        <div className={classes.details}>
          <Typography noWrap variant="subtitle1" color="textPrimary">
            {title}
          </Typography>
          <Typography noWrap variant="subtitle2" color="textSecondary">
            {stream.nickname}
          </Typography>
        </div>
      </Link>
    </div>
  );
}

Tuile.propTypes = {
  image: PropTypes.any,
  title: PropTypes.string.isRequired,
};
