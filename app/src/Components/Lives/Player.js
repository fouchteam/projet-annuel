import { StreamContext } from "../../Context/StreamContext";
import { makeStyles, Container, Button, Box } from "@material-ui/core";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import React, { useState, useEffect, useContext } from "react";
import { VideoPlayer } from "../Lib/Player/VideoPlayer";
import Typography from "@material-ui/core/Typography";
import { withAlert } from "../../Provider/AlertProvider";
import { useUser } from "../../Provider/UserProvider";
import "../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { Paper } from "@material-ui/core";
import Grid from "@material-ui/core/Grid/Grid";
import { urlConstants } from "../../_constants/url.constants";
import socketIOClient from "socket.io-client";
import { Widget, addResponseMessage } from "react-chat-widget";
import "react-chat-widget/lib/styles.css";
import "./chat.css"; // overrides widget css
import jwtDecode from "jwt-decode";
import { useFollow } from "../../Provider/FollowProvider";
import { SocialIcon } from "react-social-icons";
import PeopleIcon from "@material-ui/icons/People";
import HeadsetIcon from "@material-ui/icons/Headset";
import { red, grey } from "@material-ui/core/colors";
import { Link } from "react-router-dom";
import Banner from "../../assets/img/vinyl.jpg";
import Avatar from "../../assets/img/avatar.png";
import Loader from "../../assets/img/loader.gif";
import ReactImageFallback from "react-image-fallback";
import NotFoundUser from "../Lib/NotFoundUser";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 100,
  },
  btnFollow: {
    float: "right",
  },
  displayContainer: {
    position: "relative",
    width: "100%",
  },
  displayStream: {
    width: "100%",
  },
  displayAccountInfo: {
    marginTop: 20,
    marginBottom: 20,
    flexWrap: "wrap",
    width: "100%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  displayFlexRow: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  typoInfo: {
    marginLeft: 20,
  },
  notStreamingTypo: {
    position: "absolute",
    bottom: 20,
    left: 20,
    fontWeight: "bold",
  },
  specInfo: {
    color: red[500],
    marginRight: 20,
    marginLeft: "auto",
    display: "flex",
    alignItems: "center",
  },
  followsInfo: {
    marginRight: 10,
    marginLeft: "auto",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      color: grey[800],
    },
  },
  widget: {
    color: "black",
  },
  banner: {
    width: "100%",
    maxHeight: 650,
    objectFit: "cover",
  },
  avatar: {
    height: 50,
    width: 50,
    borderRadius: 50,
    borderColor: theme.palette.blue.primary,
    borderWidth: 2,
    borderStyle: "solid",
    objectFit: "cover",
  },
  ...DefaultClasses(theme),
}));

export const Player = withAlert(({ error, match }) => {
  // Component state
  const [stream, setStream] = useState(false);
  const [exist, setExist] = useState(true);
  const [isFollowing, setIsFollowing] = useState(null);
  const [socket, setSocket] = useState();
  const [spectators, setSpectators] = useState();
  const [videoJsOptions, setVideoJsOptions] = useState(null);

  // Global state
  const { state, retrieveUserByNickname } = useUser();
  const stateFollow = useFollow().state;
  const {
    followOneUser,
    unfollowOneUser,
    getFollowedOfCurrentUser,
  } = useFollow();

  const streamContext = useContext(StreamContext);
  const classes = useStyles();

  /**
   * New message chat
   *
   * @param {*} newMessage corresponds to a string
   */
  var handleNewUserMessage = (newMessage) => {
    const message =
      jwtDecode(localStorage.getItem("token")).nickname + ": " + newMessage;
    socket.emit("from_client", {
      data: {
        message: message,
        room: state.user.nickname,
      },
    });
  };

  // Récuperation d'information
  useEffect(() => {
    async function fetchData() {
      // Recuperation des infos du streamer pour la creation de la page de stream
      let result = await retrieveUserByNickname(match.params.username);
      setExist(result);

      // Recuperation des follows
      if (localStorage.getItem("token")) await getFollowedOfCurrentUser();
    }
    fetchData();
  }, []);

  useEffect(() => {
    setIsFollowing(null)
    if (state?.user?._id && stateFollow.followsList) {
      if (state?.user._id !== jwtDecode(localStorage.getItem("token")).id) {
        setIsFollowing(
          stateFollow.followsList.filter(
            (element) => element.followee._id === state.user._id
          ).length > 0
        );
      }
    }
  }, [stateFollow.followsList]);

  useEffect(() => {
    // First message
    if (state?.user?.nickname) {
      addResponseMessage("Welcome to " + state.user.nickname + "'s chat!");
    }

    // Set Socket
    if (!socket && state?.user?.nickname) {
      setSocket(
        socketIOClient(urlConstants.SOCKET_BASE_URL, {
          query: { room: state.user.nickname },
        })
      );
    }
  }, [state.user]);

  useEffect(() => {
    // Socket handler
    if (socket) {
      socket.on("from_server", (data) => {
        addResponseMessage(data);
      });
      socket.on("new_spectator", (data) => {
        setSpectators(data);
      });
      socket.on("spectator_disconnected", (data) => {
        setSpectators(data);
      });
      return () => socket.disconnect();
    }
  }, [socket]);

  // Get stream
  useEffect(() => {
    if (match.params && match.params.username) {
      streamContext.getUsersStream(match.params.username).catch((err) => {
        if (err.response.status !== 404)
          error("Sorry ! This user seems to present a problem");
      });
    }
  }, [match.params]);

  // Stream handler
  useEffect(() => {
    if (
      streamContext.currentStream.stream_key &&
      streamContext.currentStream.isStreaming
    ) {
      setStream(true);
      setVideoJsOptions({
        autoplay: true,
        controls: true,
        sources: [
          {
            src:
              `${process.env.REACT_APP_API_URL}:8888/live/` +
              streamContext.currentStream.stream_key +
              "/index.m3u8",
            type: "application/x-mpegURL",
          },
        ],
        fluid: true,
        responsive: true,
      });
    } else {
      setStream(false);
    }
  }, [streamContext.currentStream]);

  /**
   * Render
   */
  return (
    <Container maxWidth="lg" className={classes.root}>
      {exist && state.user && state.user._id ? (
        <div className={classes.mainContent}>
          <div className={classes.displayContainer}>
            {streamContext.currentStream.isStreaming ? (
              <div className={classes.displayStream}>
                <VideoPlayer {...videoJsOptions} />
              </div>
            ) : (
                <div>
                  <ReactImageFallback
                    src={state?.user?.url_banner}
                    fallbackImage={Banner}
                    initialImage={Loader}
                    className={classes.banner}
                  />
                  <Typography
                    variant="subtitle1"
                    className={classes.notStreamingTypo}
                  >
                    OFF
                </Typography>
                </div>
              )}
          </div>
          <Box className={classes.displayAccountInfo}>
            <Box className={classes.displayFlexRow}>
              <ReactImageFallback
                src={state.user.url_profile_picture}
                fallbackImage={Avatar}
                initialImage={Loader}
                className={classes.avatar}
              />
              <Typography variant="h6" className={classes.typoInfo}>
                {state?.user?.nickname
                  ? state.user.nickname
                  : state?.user?.display_name}
                {state?.user?.stream_name
                  ? " - " + state.user.stream_name
                  : " - Music Stream"}
              </Typography>
            </Box>
            <Box mt={1} className={classes.displayFlexRow}>
              <Typography variant="subtitle1" className={classes.specInfo}>
                <HeadsetIcon />{" "}
                <span style={{ marginLeft: 4 }}>{spectators}</span>
              </Typography>
              <Typography
                variant="subtitle1"
                className={classes.followsInfo}
                color="textSecondary"
              >
                <Link
                  to={"/follower/" + state.user.nickname}
                  style={{ textDecoration: "none", color: "inherit" }}
                  className={classes.followsInfo}
                >
                  <PeopleIcon />{" "}
                  <span style={{ marginLeft: 4 }}>
                    {state.user.followers.length}
                  </span>
                </Link>
              </Typography>
              {isFollowing != null ? (
                !isFollowing ? (
                  <Button
                    className={classes.btnFollow}
                    variant="contained"
                    color="primary"
                    size="small"
                    onClick={() => followOneUser(state.user._id)}
                  >
                    Follow
                  </Button>
                ) : (
                    <Button
                      className={classes.btnFollow}
                      variant="contained"
                      color="default"
                      size="small"
                      onClick={() => unfollowOneUser(state.user._id)}
                    >
                      Unfollow
                    </Button>
                  )
              ) : (
                  ""
                )}
            </Box>
          </Box>
          <Grid item xs={12}>
            {state.user.tags.map((tag, i) => {
              return (<Button>{tag.label}</Button>)
            })}
          </Grid>
          <Grid item xs={12}>
            <Paper style={{ minHeight: 300, padding: 20, marginTop: 10 }}>
              <div
                style={{ wordBreak: "break-word" }}
                dangerouslySetInnerHTML={{
                  __html: state.user.stream_description,
                }}
              />
            </Paper>
          </Grid>
          <Grid item xs={12}>
            <Box mt={3}>
              {state.user.instagram ? (
                <SocialIcon
                  url={state.user.instagram}
                  style={{ marginRight: 15 }}
                  target="_blank"
                />
              ) : (
                  ""
                )}
              {state.user.twitter ? (
                <SocialIcon
                  url={state.user.twitter}
                  style={{ marginRight: 15 }}
                  target="_blank"
                />
              ) : (
                  ""
                )}
              {state.user.youtube ? (
                <SocialIcon
                  url={state.user.youtube}
                  style={{ marginRight: 15 }}
                  target="_blank"
                />
              ) : (
                  ""
                )}
              {state.user.soundcloud ? (
                <SocialIcon
                  url={state.user.soundcloud}
                  style={{ marginRight: 15 }}
                  target="_blank"
                />
              ) : (
                  ""
                )}
              {state.user.tumblr ? (
                <SocialIcon
                  url={state.user.tumblr}
                  style={{ marginRight: 15 }}
                  target="_blank"
                />
              ) : (
                  ""
                )}
            </Box>
          </Grid>
          {localStorage.getItem("token") ? (
            <Grid item xs={12}>
              <Widget
                title={"#" + (state.user ? state.user.nickname : "")}
                subtitle=""
                handleNewUserMessage={handleNewUserMessage}
              />
            </Grid>
          ) : (
              ""
            )}
        </div>
      ) : (
          <div>
            {!exist ? (
              <NotFoundUser></NotFoundUser>
            ) : (
                <Typography>Loading...</Typography>
              )}
          </div>
        )}
    </Container>
  );
});
