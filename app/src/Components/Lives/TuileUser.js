import PropTypes from "prop-types";
import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Card from "@material-ui/core/Card";
import { Category } from "./Stream/Category";
import { grey } from "@material-ui/core/colors";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import Typography from "@material-ui/core/Typography";
import CardImg from "../../assets/img/vinyl.jpg";
import AvatarImg from "../../assets/img/avatar.png";
import { Link } from "@material-ui/core";
import Loader from "../../assets/img/loader.gif";
import ReactImageFallback from "react-image-fallback";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
  root: {
    width: 270,
    margin: 10,
    paddingTop: 10,
    backgroundColor: '#0d0d0d'
  },
  card: {
    display: 'flex',
    width: '100%',
    height: 150,
    backgroundColor: '#0d0d0d',
    paddingBottom: 1
  },
  details: {
    padding: 5,
    backgroundColor: '#222222',
    textAlign: 'center'
  },
  cover: {
    width: 70,
    height: 70,
    objectFit: 'cover',
    float:'right',
    marginRight:12,
    borderRadius: 70
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  description: {
    width: '100%',
    height: 140,
    paddingLeft: 12,
    paddingRight: 6,
    textAlign:'center',
    overflow: 'hidden',
    display: '-webkit-box',
    textOverflow: 'ellipsis',
    WebkitBoxOrient: 'vertical',
    WebkitLineClamp: 5
  },
}));

export function TuileUser({
  user = {
    _id: 0,
    nickname: "Bren",
  }
}) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Link
        href={"/stream/" + user.nickname}
        style={{ textDecoration: "none", color: "inherit" }}
      >
        <Card className={classes.card}>
          <Typography variant="subtitle1" color="textPrimary" className={classes.description}>
            {user.description || 'Bienvenue sur mon stream !'}
          </Typography>
          <ReactImageFallback
            src={ user.url_profile_picture }
            fallbackImage={ AvatarImg }
            initialImage={Loader}
            className={classes.cover}
          >{user.tags ? (
            user.tags.map((tag) => {
              return (
                <Category key={tag} color={grey} name={tag.value} />
              );
            })
          ) : (
              <></>
            )}</ReactImageFallback>
        </Card>
        <div className={classes.details}>
          <Typography noWrap variant="subtitle1" color="textPrimary">
            {user.nickname}
          </Typography>
        </div>
      </Link>
    </div>
  );
}

TuileUser.propTypes = {
  user: PropTypes.object,
};
