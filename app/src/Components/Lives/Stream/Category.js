import React from "react";
import { withStyles } from "@material-ui/core";
import Button from "@material-ui/core/Button";

const styles = (theme) => ({
  root: {
    color: (props) => theme.palette.getContrastText(props.color[900]),
    backgroundColor: (props) => props.color[900],
    "&:hover": {
      backgroundColor: (props) => props.color[800],
    },
    borderRadius: 5,
    paddingLeft: theme.spacing(1),
    paddingRight: theme.spacing(1),
    paddingTop: theme.spacing(0),
    paddingBottom: theme.spacing(0),
    textTransform: "none",
    alignItems: "center",
  },
});

function ButtonRaw(props) {
  const { classes, color, ...other } = props;
  return <Button className={classes.root} {...other} />;
}

const ColorButton = withStyles(styles)(ButtonRaw);

export const Category = ({ color, name, handleClick }) => (
  <ColorButton color={color} onClick={handleClick}>
    {name}
  </ColorButton>
);
