import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";
import {DefaultClasses} from "../../../_helper/DefaultClasses";

const useStyle = makeStyles(theme => ({
    ...DefaultClasses(theme)
}));

export const InfoStream = ({user, title}) => {
    const classes = useStyle();

    return (
        <div className={classes.rowCenter}>
            <Avatar src={user.profilImg}/>
            <div className={`${classes.columnCenter} ${classes.paddingLeft}`}>
                <Typography component="h5" variant="h5">{title}</Typography>
                <Typography variant="subtitle1">{user.pseudo}</Typography>
            </div>
        </div>
    );
};