import React from "react";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";
import { red } from "@material-ui/core/colors";
import { makeStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography/Typography";

const useStyles = makeStyles((theme) => ({
  spectator: {
    display: "flex",
    flexDirection: "row",
  },
}));

export const Spectator = ({ nbSpectator }) => {
  const classes = useStyles();

  return (
    <div className={classes.spectator}>
      <FiberManualRecordIcon style={{ color: red[500] }} />
      <Typography component="h6">{nbSpectator}</Typography>
    </div>
  );
};
