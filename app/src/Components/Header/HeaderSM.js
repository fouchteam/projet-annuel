import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { Link } from "react-router-dom";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import Button from "@material-ui/core/Button/Button";
import TunelLogo from "../../assets/img/TUNEL_LOGO_SIMPLE.png";
import LockOpenIcon from "@material-ui/icons/LockOpen";
import { Menu, MenuItem } from "@material-ui/core";
import { AuthContext } from "../../Context/AuthContext";
import { useAccount } from "../../Provider/AccountProvider";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
  grow: {
    flexGrow: 1,
  },
  root: {
    boxShadow: "none",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textAlign: "left",
  },
  greet: {
    flexGrow: 1,
    textAlign: "right",
  },
  link: {
    textDecoration: "none",
  },
  marginLeft: {
    marginLeft: theme.spacing(3),
  },
}));

export default function HeaderSM() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const classes = useStyles();
  const authContext = useContext(AuthContext);
  const { state } = useAccount();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className={classes.grow}>
      <AppBar position="fixed" color="inherit">
        <Toolbar>
          <Link to={"/"}>
            <img src={TunelLogo} alt="Tunel Icon" width={50} />
          </Link>

          <Link to={"/search"} className={`${classes.link} ${classes.marginLeft}`}>
            <Typography variant="h6" color="textPrimary">
              SEARCH
            </Typography>
          </Link>

          <div className={classes.grow} />
          {authContext.state.isLogged &&
            authContext.state.token &&
            state.currentUser ? (
              <>

                {state.currentUser ? (
                  <Link
                    to={"/stream/" + state.currentUser.nickname}
                    style={{ textDecoration: "none" }}
                  >
                    <Typography variant="subtitle1" color="textPrimary">
                      {state.currentUser.nickname}
                    </Typography>
                  </Link>
                ) : (
                    ""
                  )}

                <IconButton
                  aria-label="account of current user"
                  aria-controls="menu-appbar"
                  aria-haspopup="true"
                  color="primary"
                >
                  <Link to="/account">
                    <AccountCircle className={classes.link} color="primary" />
                  </Link>
                </IconButton>
              </>
            ) : (
              <>
                <div>
                  <Button
                    aria-controls="simple-menu"
                    aria-haspopup="true"
                    onClick={handleClick}
                  >
                    <LockOpenIcon></LockOpenIcon>
                  </Button>
                  <Menu
                    id="simple-menu"
                    anchorEl={anchorEl}
                    keepMounted
                    open={Boolean(anchorEl)}
                    onClose={handleClose}
                  >
                    <MenuItem onClick={handleClose}>
                      <Link to="/login" style={{ textDecoration: "none" }}>
                        <Typography variant="button" color="textPrimary">
                          Login
                      </Typography>
                      </Link>
                    </MenuItem>
                    <MenuItem onClick={handleClose}>
                      <Link to="/register" style={{ textDecoration: "none" }}>
                        <Typography variant="button" color="textSecondary">
                          Register
                      </Typography>
                      </Link>
                    </MenuItem>
                  </Menu>
                </div>
              </>
            )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
