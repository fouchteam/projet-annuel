import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import { Link } from "react-router-dom";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import Button from "@material-ui/core/Button/Button";
import TunelLogo from "../../assets/img/TUNEL_LOGO_DARK_LARGE.png";
import { AuthContext } from "../../Context/AuthContext";
import ReactImageFallback from "react-image-fallback";
import Avatar from "../../assets/img/avatar.png";
import Loader from "../../assets/img/loader.gif";
import { useAccount } from "../../Provider/AccountProvider";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
  grow: {
    flexGrow: 1,
  },
  root: {
    boxShadow: "none",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    textAlign: "left",
  },
  greet: {
    flexGrow: 1,
    textAlign: "right",
  },
  link: {
    textDecoration: "none",
  },
  marginLeft: {
    marginLeft: theme.spacing(3),
  },
  avatar: {
    height: 35,
    width: 35,
    borderRadius: 50,
    borderColor: theme.palette.blue.primary,
    borderWidth: 2,
    borderStyle: "solid",
    objectFit: "cover",
  },
}));

export default function HeaderLG() {
  const classes = useStyles();
  const authContext = useContext(AuthContext);
  const { state } = useAccount();

  useEffect(() => { }, [state.currentUser]);

  return (
    <div className={classes.grow}>
      <AppBar position="fixed" color="inherit">
        <Toolbar>
          <Link to={"/"}>
            <img src={TunelLogo} alt="Tunel Icon" width={125} />
          </Link>

          <Link to={"/search"} className={`${classes.link} ${classes.marginLeft}`}>
            <Typography variant="h6" color="textPrimary">
              SEARCH
            </Typography>
          </Link>

          <div className={classes.grow} />
          {authContext.state.isLogged && authContext.state.token ? (
            <>
              {state.currentUser ? (
                <Link
                  to={"/stream/" + state.currentUser.nickname}
                  style={{ textDecoration: "none" }}
                >
                  <Typography variant="h6" color="textPrimary">
                    {state.currentUser.nickname}
                  </Typography>
                </Link>
              ) : (
                  ""
                )}

              <IconButton
                aria-label="account of current user"
                aria-controls="menu-appbar"
                aria-haspopup="true"
                color="inherit"
              >
                <Link to="/account">
                  <ReactImageFallback
                    src={
                      state.currentUser
                        ? state.currentUser.url_profile_picture
                        : null
                    }
                    fallbackImage={Avatar}
                    initialImage={Loader}
                    className={classes.avatar}
                  ></ReactImageFallback>{" "}
                </Link>
              </IconButton>
            </>
          ) : (
              <Typography>
                <Link to="/login" style={{ textDecoration: "none" }}>
                  <Button>Login</Button>
                </Link>
                <Link to="/register" style={{ textDecoration: "none" }}>
                  <Button>Register</Button>
                </Link>
              </Typography>
            )}
        </Toolbar>
      </AppBar>
    </div>
  );
}
