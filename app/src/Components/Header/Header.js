import React from 'react';
import { withWidth, isWidthUp } from "@material-ui/core";
import HeaderLG from './HeaderLG';
import HeaderSM from './HeaderSM';

function Header(props) {
  if (isWidthUp('sm', props.width)) {
    return <HeaderLG></HeaderLG>
  }

  return <HeaderSM></HeaderSM>;
}

export default withWidth()(Header);