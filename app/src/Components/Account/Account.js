import React from 'react';

import { Container, Tabs, Tab, Box } from '@material-ui/core';
import AccountForm from "./AccountForm";
import StreamForm from './StreamForm';

const Account = () => {
    const [value, setValue] = React.useState(0);

    const handleChange = (_, newValue) => {
        setValue(newValue);
    };

    return (
        <Container maxWidth="lg">
            <Box mt={15} mb={5}>
                <Tabs
                    value={value}
                    indicatorColor="primary"
                    onChange={handleChange}
                >
                    <Tab label="Informations" />
                    <Tab label="Stream" />
                </Tabs>
            </Box>

            {value === 0 && (
                <AccountForm />
            )}
            {value === 1 && (
                <StreamForm />
            )}
        </Container>
    )
};

export default Account;