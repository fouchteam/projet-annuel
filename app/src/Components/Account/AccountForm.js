import React, { useEffect, useContext } from 'react';

import { useFormik } from 'formik';
import TextField from "@material-ui/core/TextField/TextField";
import Grid from "@material-ui/core/Grid/Grid";
import Button from "@material-ui/core/Button/Button";
import Box from "@material-ui/core/Box/Box";
import Typography from "@material-ui/core/Typography/Typography";
import { useAccount } from "../../Provider/AccountProvider";
import { AuthContext } from '../../Context/AuthContext';
import { useHistory } from 'react-router-dom';
import { endpointConstants } from '../../_constants/endpoint.constants';
import Avatar from "../../assets/img/avatar.png";
import Banner from "../../assets/img/vinyl.jpg";
import Loader from "../../assets/img/loader.gif";
import ReactImageFallback from "react-image-fallback";
import { makeStyles, Tabs, Tab } from '@material-ui/core';
import { DefaultClasses } from '../../_helper/DefaultClasses';


const useStyles = makeStyles((theme) => ({
    avatar: {
        height: 50,
        width: 50,
        borderRadius: 50,
        borderColor: theme.palette.blue.primary,
        borderWidth: 2,
        borderStyle: 'solid',
        objectFit: 'cover'
    },
    banner: {
        height: 100,
        width: 150,
        borderRadius: 8,
        borderColor: theme.palette.blue.primary,
        borderWidth: 2,
        borderStyle: 'solid'
    },
    pictureGrid: {
        display: 'flex',
        alignItems: 'center'
    },
    previewGrid: {
        textAlign: "center"
    },
    ...DefaultClasses(theme),
}));


const AccountForm = () => {
    const { state, retrieveCurrentUser, updateCurrentUser, updateCurrentUserPassword } = useAccount();
    const authContext = useContext(AuthContext);
    const history = useHistory();
    const classes = useStyles();
    const [value, setValue] = React.useState(0);

    // USE AN HOOK TO LOAD INITIAL VALUES
    const formik = useFormik({
        initialValues: {
            nickname: "",
            email: "",
            description: "",
            display_name: "",
            url_profile_picture: "",
            url_banner: "",
            instagram: "",
            twitter: "",
            youtube: "",
            soundcloud: "",
            facebook: "",
            tumblr: "",
            password: ""
        },
        onSubmit: async values => {
            await updateCurrentUser(values);
            authContext.setIsTokenUpdated();
        },
        enableReinitialize: false
    });

    const passwordFormik = useFormik({
        initialValues: {
            password: ""
        },
        onSubmit: async values => {
            await updateCurrentUserPassword(values);
        },
        enableReinitialize: false
    });

    useEffect(() => {
        retrieveCurrentUser();
        passwordFormik.setValues({
            password: "",
            confirmation: ""
        })
    }, [])

    useEffect(() => {
        if (state.currentUser) {
            formik.setValues({
                nickname: state.currentUser.nickname || "",
                email: state.currentUser.email || "",
                description: state.currentUser.description || "",
                display_name: state.currentUser.display_name || "",
                url_profile_picture: state.currentUser.url_profile_picture || "",
                url_banner: state.currentUser.url_banner || "",
                instagram: state.currentUser.instagram || "",
                twitter: state.currentUser.twitter || "",
                youtube: state.currentUser.youtube || "",
                soundcloud: state.currentUser.soundcloud || "",
                facebook: state.currentUser.facebook || "",
                tumblr: state.currentUser.tumblr || ""
            })
        }
    }, [state.currentUser])

    const handleLogout = async () => {
        await authContext.logout()
        history.push(endpointConstants.LOGIN_ENDPOINT)
    }

    const handleChange = (_, newValue) => {
        setValue(newValue);
    };

    function validateForm() {
        return ((passwordFormik.values.password === passwordFormik.values.confirmation) && passwordFormik.values.password !== "");
    }

    return (
        <>
            <form onSubmit={formik.handleSubmit} autoComplete="off">
                <Tabs
                    value={value}
                    indicatorColor="secondary"
                    onChange={handleChange}
                >
                    <Tab label="Account" />
                    <Tab label="Social Media" />
                    <Tab label="Password" />
                </Tabs>
                {value === 0 && (
                    <div>
                        <Box marginBottom={5} marginTop={5}>
                            <Typography variant="h6">Login informations</Typography>
                        </Box>
                        <Box mb={5}>
                            <Grid container spacing={4}>
                                <Grid item lg={6} md={6} sm={12} xs={12}>
                                    <TextField
                                        id="nickname"
                                        label="Nickname"
                                        variant="outlined"
                                        name="nickname"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.nickname}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item lg={6} md={6} sm={12} xs={12}>
                                    <TextField
                                        id="email"
                                        label="Email"
                                        variant="outlined"
                                        name="email"
                                        type="email"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.email}
                                        fullWidth
                                        disabled
                                    />
                                </Grid>
                            </Grid>
                            <Box marginBottom={5} marginTop={5}>
                                <Typography variant="h6">Stage name and description</Typography>
                            </Box>
                            <Grid container spacing={4}>
                                <Grid item lg={6} md={6} sm={12} xs={12}>
                                    <TextField
                                        id="display_name"
                                        label="Display name"
                                        variant="outlined"
                                        name="display_name"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.display_name}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        multiline
                                        rows={5}
                                        id="description"
                                        variant="outlined"
                                        label="Description"
                                        name="description"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.description}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                            <Box marginBottom={5} marginTop={5}>
                                <Typography variant="h6">Appearance</Typography>
                            </Box>
                            <Grid container spacing={4}>
                                <Grid item xs={12} sm={8} className={classes.pictureGrid}>
                                    <TextField
                                        id="url_profile_picture"
                                        label="Avatar"
                                        variant="outlined"
                                        placeholder="Enter an URL"
                                        name="url_profile_picture"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.url_profile_picture}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12} sm={4} className={classes.previewGrid}>
                                    <ReactImageFallback
                                        src={formik.values.url_profile_picture}
                                        fallbackImage={Avatar}
                                        initialImage={Loader}
                                        className={classes.avatar}
                                    />
                                </Grid>
                                <Grid item xs={12} sm={8} className={classes.pictureGrid}>
                                    <TextField
                                        id="url_banner"
                                        label="Banner"
                                        variant="outlined"
                                        name="url_banner"
                                        placeholder="Enter an URL"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.url_banner}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12} sm={4} className={classes.previewGrid}>
                                    <ReactImageFallback
                                        src={formik.values.url_banner}
                                        fallbackImage={Banner}
                                        initialImage={Loader}
                                        className={classes.banner}
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                        <Grid item xs={3}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={formik.handleSubmit}
                            >Save</Button>
                        </Grid>
                    </div>
                )}
                {value === 1 && (
                    <div>
                        <Box marginBottom={5} marginTop={5}>
                            <Typography variant="h6">Share your social media accounts on your profile and your stream</Typography>
                        </Box>
                        <Box mb={10}>
                            <Grid container spacing={4}>
                                <Grid item xs={12}>
                                    <TextField
                                        id="instagram"
                                        label="Instragram"
                                        variant="outlined"
                                        name="instagram"
                                        type="text"
                                        size="small"
                                        placeholder="Enter an URL"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.instagram}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="twitter"
                                        label="Twitter"
                                        variant="outlined"
                                        placeholder="Enter an URL"
                                        name="twitter"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.twitter}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="youtube"
                                        variant="outlined"
                                        label="Youtube"
                                        placeholder="Enter an URL"
                                        name="youtube"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.youtube}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="soundcloud"
                                        label="Soundcloud"
                                        placeholder="Enter an URL"
                                        variant="outlined"
                                        name="soundcloud"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.soundcloud}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="facebook"
                                        label="Facebook"
                                        placeholder="Enter an URL"
                                        variant="outlined"
                                        name="facebook"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.facebook}
                                        fullWidth
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        id="tumblr"
                                        label="Tumblr"
                                        placeholder="Enter an URL"
                                        variant="outlined"
                                        name="tumblr"
                                        type="text"
                                        size="small"
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                        onChange={formik.handleChange}
                                        value={formik.values.tumblr}
                                        fullWidth
                                    />
                                </Grid>
                            </Grid>
                        </Box>
                        <Grid item xs={3}>
                            <Button
                                variant="contained"
                                color="primary"
                                onClick={formik.handleSubmit}
                            >Save</Button>
                        </Grid>
                    </div>
                )}
            </form>
            {value === 2 && (
                <form onSubmit={passwordFormik.handleSubmit} autoComplete="off">
                    <Box marginBottom={5} marginTop={5}>
                        <Typography variant="h6">Modify your password</Typography>
                    </Box>
                    <Box mb={5}>
                        <Grid container spacing={4}>
                            <Grid item xs={12}>
                                <TextField
                                    id="password"
                                    label="New Password"
                                    variant="outlined"
                                    name="password"
                                    type="password"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={passwordFormik.handleChange}
                                    fullWidth
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <TextField
                                    id="confirmation"
                                    label="Confirmation"
                                    variant="outlined"
                                    name="confirmation"
                                    type="password"
                                    size="small"
                                    InputLabelProps={{
                                        shrink: true,
                                    }}
                                    onChange={passwordFormik.handleChange}
                                    fullWidth
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Grid item xs={3}>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={passwordFormik.handleSubmit}
                            disabled={!validateForm()}
                        >Save</Button>
                    </Grid>
                </form>
            )}
            <Box marginBottom={5} marginTop={5}>
                <Button variant="contained" color="secondary" onClick={handleLogout}>Logout</Button>
            </Box>
        </>

    );
};

export default AccountForm;
