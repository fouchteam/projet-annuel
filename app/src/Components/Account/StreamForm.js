import React, { useEffect } from 'react';
import { useFormik } from 'formik';
import TextField from "@material-ui/core/TextField/TextField";
import Select from 'react-select';
import makeAnimated from 'react-select/animated';
import { tags } from '../../assets/tags';
import Grid from "@material-ui/core/Grid/Grid";
import Button from "@material-ui/core/Button/Button";
import Box from "@material-ui/core/Box/Box";
import Typography from "@material-ui/core/Typography/Typography";
import { useAccount } from "../../Provider/AccountProvider";
import { StreamKeyGen } from "../Lib/User/StreamKeyGen";
import { EditorState, ContentState, convertToRaw } from "draft-js";
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import '../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { makeStyles, Paper } from '@material-ui/core';
import InputLabel from '@material-ui/core/InputLabel';


const useStyles = makeStyles((theme) => ({
    wysiwygEditor: {
        borderColor: theme.palette.border.main,
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 4,
        minHeight: 300,
        padding: 20
    },
    editorLabel: {
        fontSize: 12,
        marginBottom: 8,
        marginLeft: 15,
        fontWeight: 500
    }
  }));

const StreamForm = () => {
    const classes = useStyles();
    const { state, retrieveCurrentUser, updateCurrentUser, generateStreamKey } = useAccount();
    const [editorState, setEditorState] = React.useState(EditorState.createEmpty());
    const [tagsState, setTagsState] = React.useState([])
    const animatedComponents = makeAnimated();

    const tagsStyles = {
        container: (styles, {isFocused}) => {
            return {
                ...styles,
                borderRadius: '4px',
                border: isFocused ? '2px solid #2684ff' : '1px solid #4a4a4a',
            };
        },
        control: (styles) => {
            return {
                ...styles,
                backgroundColor: '#131313',
                border: 'none',
            };
        },
        input: (styles) => {
            return {
                ...styles,
                color: 'white',
            };
        },
        option: (styles, {isFocused}) => {
            return {
                ...styles,
                backgroundColor: 'white',
                color: isFocused ? '#0064c7' : '#000',
            };
        },
        multiValue: (styles) => {
            return {
                ...styles,
                color: '#FFF',
                backgroundColor: '#0064c7',
            };
        },
        multiValueLabel: (styles) => {
            return {
                ...styles,
                color: '#FFF',
            };
        },
    };

    const onChangeEditor = (editorState) => {
        setEditorState(editorState)
    }

    const onChangeTags = (tagsState) => {
        setTagsState(tagsState)
    }

    // USE AN HOOK TO LOAD INITIAL VALUES
    const formik = useFormik({
        initialValues: {
            stream_name: "",
            tags: [],
        },
        onSubmit: values => {
            values.stream_description = draftToHtml(convertToRaw(editorState.getCurrentContent()));
            values.tags = tagsState

            updateCurrentUser(values);
        },
    });

    useEffect(() => {
        retrieveCurrentUser()
    }, [])

    useEffect(() => {
        if (state.currentUser) {
            formik.setValues({
                stream_name: state.currentUser.stream_name || ""
            })
            const blocksFromHtml = htmlToDraft(state.currentUser.stream_description || "");
            const { contentBlocks, entityMap } = blocksFromHtml;
            const contentState = ContentState.createFromBlockArray(contentBlocks, entityMap);
            setEditorState(EditorState.createWithContent(contentState));
            setTagsState(state.currentUser.tags)
        }
    }, [state.currentUser])

    return (
        <>
            <Box marginBottom={5}>
                <Typography variant="h6">Stream</Typography>
                <Typography variant="caption">Change your informations about your stream</Typography>
            </Box>
            <Box mb={10}>
                <form onSubmit={formik.handleSubmit} autoComplete="off">
                    <Grid container spacing={4}>
                        <Grid item xs={12}>
                            <TextField
                                id="stream_name"
                                label="Stream name"
                                variant="outlined"
                                name="stream_name"
                                type="text"
                                size="small"
                                InputLabelProps={{
                                    shrink: true,
                                }}
                                onChange={formik.handleChange}
                                value={formik.values.stream_name}
                                fullWidth
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <InputLabel className={classes.editorLabel}>Stream tags</InputLabel>
                            <Select
                                id="tags"
                                name="tags"
                                closeMenuOnSelect={false}
                                components={animatedComponents}
                                isMulti
                                options={tags}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                styles={tagsStyles}
                                value={tagsState}
                                onChange={onChangeTags}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <InputLabel className={classes.editorLabel}>Stream description</InputLabel>
                            <Editor
                                editorState={editorState}
                                wrapperClassName={classes.wysiwygWrapper}
                                editorClassName={classes.wysiwygEditor}
                                onEditorStateChange={onChangeEditor}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <InputLabel className={classes.editorLabel}>Preview</InputLabel>
                            <Paper style={{ minHeight: 300, padding: 20 }}>
                            <div
                                style={{ wordBreak: "break-word" }}
                                dangerouslySetInnerHTML={{
                                __html: draftToHtml(convertToRaw(editorState.getCurrentContent()))
                                }}
                            />
                            </Paper>
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" color="primary" onClick={formik.handleSubmit}>Save</Button>
                        </Grid>
                    </Grid>
                </form>
            </Box>
            {state.currentUser && state.currentUser.stream_key ? <StreamKeyGen regenStreamKey={generateStreamKey}
                streamKey={state.currentUser.stream_key} /> : "Failed to load "}
        </>

    );
};

export default StreamForm;
