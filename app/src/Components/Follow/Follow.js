import React, { useEffect } from "react";

import {
  Container,
  Typography,
  Box,
  Grid,
  makeStyles,
} from "@material-ui/core";
import { useUser } from "../../Provider/UserProvider";
import { Link } from "react-router-dom";
import NotFoundUser from "../Lib/NotFoundUser";
import ReactImageFallback from "react-image-fallback";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import Avatar from "../../assets/img/avatar.png";
import Loader from "../../assets/img/loader.gif";

const useStyles = makeStyles((theme) => ({
  root: {
    marginTop: 100,
  },
  banner: {
    width: "100%",
    height: 650,
    objectFit: "cover",
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 50,
    borderColor: theme.palette.blue.primary,
    borderWidth: 2,
    borderStyle: "solid",
    objectFit: "cover",
  },
  displayFlexRow: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  },
  ...DefaultClasses(theme),
}));

const Follow = (props) => {
  const { state, retrieveUserByNickname } = useUser();
  const nickname = props.match.params.username;
  const classes = useStyles();

  // Récuperation d'information
  useEffect(() => {
    async function fetchData() {
      await retrieveUserByNickname(nickname);
    }
    fetchData();
  }, []);

  return (
    <Container maxWidth="lg">
      {state.user && state.user._id ? (
        <>
          <Box mt={15} mb={5}>
            <Typography variant="h6">Follower of {nickname}</Typography>
          </Box>
          <Box mt={5} mb={5}>
            <Grid container>
              <Grid item lg={12}>
                {state.user.followers.map((follower) => {
                  return (
                    <Box key={follower._id} className={classes.displayFlexRow} mb={3}>
                      <ReactImageFallback
                        src={follower.follower.url_profile_picture}
                        fallbackImage={Avatar}
                        initialImage={Loader}
                        className={classes.avatar}
                      />
                      <Box ml={4}>
                        <Link
                          to={"/stream/" + follower.follower.nickname}
                          style={{ textDecoration: "none" }}
                          className={classes.typoInfo}
                        >
                          <Typography variant="h6" color="textPrimary">
                            {follower.follower?.display_name
                              ? follower.follower.nickname +
                              " " +
                              follower.follower.display_name
                              : follower.follower.nickname}
                          </Typography>
                        </Link>
                      </Box>
                    </Box>
                  );
                })}
              </Grid>
            </Grid>
          </Box>
        </>
      ) : (
          <NotFoundUser></NotFoundUser>
        )}
    </Container>
  );
};

export default Follow;
