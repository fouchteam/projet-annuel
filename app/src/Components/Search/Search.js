import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import { TuileUser } from "../Lives/TuileUser";
import { Container, Box, Grid, TextField, Button } from "@material-ui/core";
import { useFormik } from "formik";
import { useUser } from "../../Provider/UserProvider";
import NotFoundUser from "../Lib/NotFoundUser";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
}));

function Search() {
  const { state, retrieveUsersByNicknameLike } = useUser();
  const [isSubmitted, setIsSubmitted] = React.useState(false);

  const formik = useFormik({
    initialValues: {
      username: "",
    },
    onSubmit: async (values) => {
      state.userList = [];
      setIsSubmitted(true);
      await retrieveUsersByNicknameLike(values.username);
    },
  });

  return (
    <div>
      <Container
        component="main"
        maxWidth="lg"
        style={{ marginTop: 120, textAlign: "center" }}
      >
        <Grid container justify="center">
          <form
            onSubmit={formik.handleSubmit}
            autoComplete="off"
            style={{ width: "100%", marginBottom: 50 }}
          >
            <Grid container justify="center" spacing={1}>
              <Grid item xs={5}>
                <TextField
                  id="username"
                  placeholder="Search by User"
                  variant="outlined"
                  name="username"
                  type="text"
                  size="small"
                  InputLabelProps={{
                    shrink: true,
                  }}
                  fullWidth
                  onChange={formik.handleChange}
                />
              </Grid>
              <Grid item>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={formik.handleSubmit}
                >
                  Search
                </Button>
              </Grid>
            </Grid>
          </form>
          <Grid container justify="center" spacing={3}>
            {state?.userList?.map((user, _) => {
              return (
                <Grid item key={user._id}>
                  <TuileUser key={user._id} user={user}/>
                </Grid>
              );
            })}
            {state.userList.length === 0 && isSubmitted ? (
              <NotFoundUser></NotFoundUser>
            ) : (
              ""
            )}
          </Grid>
        </Grid>
      </Container>
    </div>
  );
}

export default Search;
