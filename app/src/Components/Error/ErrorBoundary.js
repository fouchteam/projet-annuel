import React from "react";
import Typography from "@material-ui/core/Typography";
import {StaticAlert} from "../Lib/StaticAlert";

export default class ErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        // Mettez à jour l'état, de façon à montrer l'UI de repli au prochain rendu.
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        // Vous pouvez aussi enregistrer l'erreur au sein d'un service de rapport.
        console.log(error, errorInfo);
    }

    render() {
        if (this.state.hasError) {
            // Vous pouvez afficher n'importe quelle UI de repli.
            return <div style={{marginTop: 90}}><StaticAlert severity={"error"}><Typography>Something went wrong</Typography></StaticAlert></div>;
        }

        return this.props.children;
    }
}
