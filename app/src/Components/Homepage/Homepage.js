import "@brainhubeu/react-carousel/lib/style.css";

import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { DefaultClasses } from "../../_helper/DefaultClasses";
import Typography from "@material-ui/core/Typography/Typography";
import { Tuile } from "../Lives/Tuile";
import { TuileUser } from "../Lives/TuileUser";
import {
  Container,
  withWidth,
  isWidthDown,
  Box,
  Tabs,
  Tab,
} from "@material-ui/core";
import { StreamContext } from "../../Context/StreamContext";
import Carousel from "@brainhubeu/react-carousel";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { useFollow } from "../../Provider/FollowProvider";
import { Link } from "react-router-dom";
import { AuthContext } from "../../Context/AuthContext";
import { useUser } from "../../Provider/UserProvider";

const useStyles = makeStyles((theme) => ({
  ...DefaultClasses(theme),
  containerMain: {
    display: "flex",
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "center",
  },
  carousel: {
    paddingBottom: "25px",
  },
  titleMain: {
    marginTop: "40px",
    marginBottom: "10px",
  },
  banner: {
    marginBottom: "50px",
    width: "100%",
  },
  bannerImage: {
    maxWidth: "100%",
    maxHeight: "100%",
    borderRadius: "5px",
  },
  preHeader: {
    background:
      "linear-gradient(90deg, rgba(0,138,255,1) 0%, rgba(0,211,255,1) 53%, rgba(0,255,184,1) 100%)",
    paddingTop: 50,
    top: 0,
  },
}));

function Homepage(props) {
  const classes = useStyles();
  const context = useContext(StreamContext);
  const authContext = useContext(AuthContext);
  const { state, getFollowedOfCurrentUser } = useFollow();
  const userState = useUser().state;
  const { retrieveUserHomepage } = useUser();

  // Onglets
  const [value, setValue] = React.useState(0);
  const handleChange = (_, newValue) => {
    setValue(newValue);
  };

  // Trigger retrieving
  useEffect(() => {
    retrieveUserHomepage();
  }, []);

  // Trigger retrieving
  useEffect(() => {
    if (authContext.state.isLogged && authContext.state.token)
      getFollowedOfCurrentUser();
  }, [authContext.state]);

  useEffect(() => {
    context.getLiveStreams();
  }, []);

  return (
    <div className={classes.root}>
      { userState?.usersHomepage?.length > 0 ? (
      <div className={classes.preHeader}>
        <Container component="main" maxWidth="lg" className={classes.titleMain}>
          <Typography variant="h6">
            Independent music streaming service
          </Typography>
        </Container>
        <Container component="main" maxWidth="lg" className={classes.carousel}>
          {/* TODO: Replacez avec le composant de présentation d'utilisateurs et non pas de musique ! */}
          {isWidthDown("md", props.width) ? (
            <Carousel slidesPerPage={1} dots infinite>
              {
                userState.usersHomepage.map((user, _) => {
                  return <TuileUser key={user._id} user={user} />
              }) }
            </Carousel>
          ) : (
            <Carousel
              slidesPerPage={3}
              arrows
              infinite
              arrowLeft={<ChevronLeftIcon />}
              arrowLeftDisabled={<ArrowLeftIcon />}
              arrowRight={<ChevronRightIcon />}
              arrowRightDisabled={<ArrowRightIcon />}
              addArrowClickHandler
            >
              { userState.usersHomepage.map((user, _) => {
                return <TuileUser key={user._id} user={user} />
              })}
            </Carousel>
          )}
        </Container>
      </div>) : <Box mt={10}></Box> }

      <Container component="main" maxWidth="lg">
        <Box mt={5} mb={5}>
          <Tabs value={value} indicatorColor="primary" onChange={handleChange}>
            <Tab label="STREAMS ON AIR" />
            <Tab label="FOLLOWING CHANNELS" />
          </Tabs>
        </Box>

        {value === 0 && (
          <Box>
            <Box className={classes.containerMain}>
              {context.liveStreams.length > 0 ? (
                context.liveStreams.map((stream, _) => (
                  <Tuile
                    key={stream._id}
                    stream={stream}
                    title={stream.stream_name}
                    tags={stream.tags}
                  />
                ))
              ) : (
                <Typography variant="h6">No stream available</Typography>
              )}
            </Box>
          </Box>
        )}
        {value === 1 && (
          <Box>
            {authContext.state.isLogged && authContext.state.token ? (
              <Box className={classes.containerMain}>
                {state.followsList?.length > 0 ? (
                  state.followsList.map((mapuser, _) => (
                    <TuileUser key={mapuser._id} user={mapuser.followee} />
                  ))
                ) : (
                  <Typography variant="h6">
                    You're not following anybody
                  </Typography>
                )}
              </Box>
            ) : (
              <Typography variant="h6">You're not logged to the app</Typography>
            )}
          </Box>
        )}
      </Container>
    </div>
  );
}

export default withWidth()(Homepage);
