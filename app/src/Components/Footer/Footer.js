import React from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import { DefaultClasses } from "../../_helper/DefaultClasses";

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {"Copyright © "}
      Tunel
      {" " + new Date().getFullYear()}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  footer: {
    ...DefaultClasses(theme),
    margin: theme.spacing(7),
    padding: theme.spacing(5, 2),
  },
  borderTypo: {
    borderTop: "1px solid",
    borderColor: theme.palette.background.paper,
    padding: theme.spacing(3),
  },
}));

export default function Footer() {
  const classes = useStyles();

  return (
    <footer color="inherit" className={classes.footer}>
      <Container maxWidth="sm" className={classes.borderTypo}>
        <Copyright />
      </Container>
    </footer>
  );
}
