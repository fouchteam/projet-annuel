export const urlConstants = {
    API_MEDIA_SERVER: `${process.env.REACT_APP_API_URL}:8888/api/`,
    API_BASE_URL: `${process.env.REACT_APP_API_URL}` + (process.env.NODE_ENV === "production" ? "" : ":3000"),
    SOCKET_BASE_URL: `${process.env.REACT_APP_API_URL}:4000`,
    RTMP_SERVER_URL: `${process.env.REACT_APP_RTMP_URL}`
};