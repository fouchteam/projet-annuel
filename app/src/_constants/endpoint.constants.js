export const endpointConstants = {
    REGISTER_ENDPOINT: "/register",
    LOGIN_ENDPOINT: "/login",
    USER_ENDPOINT: "/users",
    PASSWORD_ENDPOINT: "/password",
    GEN_STREAM_KEY: "/stream_key"
};