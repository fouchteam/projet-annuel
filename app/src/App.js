import React from "react";
import "./App.css";
import { Root } from "./Components/Root";
import Alert from "./Components/Lib/Alert";
import { AlertProvider } from "./Provider/AlertProvider";
import { CookiesProvider } from "react-cookie";
import { AuthProvider } from "./Provider/AuthProvider";
import { MuiThemeProvider, CssBaseline } from "@material-ui/core";
import { muiTheme } from "./_theme/MuiTheme";
import { UserProvider } from "./Provider/UserProvider";
import { AccountProvider } from "./Provider/AccountProvider";

function App() {
  return (
    <div className="App">
      <MuiThemeProvider theme={muiTheme}>
        <CssBaseline />
        <CookiesProvider>
          <AlertProvider>
          <AuthProvider>
            <AccountProvider>
              <UserProvider>
                <Root />
                <Alert />
              </UserProvider>
            </AccountProvider>
            </AuthProvider>
          </AlertProvider>
        </CookiesProvider>
      </MuiThemeProvider>
    </div>
  );
}

export default App;
