export const DefaultClasses = (theme) => ({
    rowCenter: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        display: "flex",
    },
    columnCenter: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "flex-start",
        alignItems: "flex-start"
    },
    paddingLeft: {
        paddingLeft: theme.spacing(1)
    },
    paddingRight: {
        paddingRight: theme.spacing(1)
    },
    paddingTop: {
        paddingTop: theme.spacing(1)
    },
    paddingBottom: {
        paddingBottom: theme.spacing(1)
    },
    padding: {
        padding: theme.spacing(1)
    },
    marginLeft: {
        marginLeft: theme.spacing(1)
    },
    marginRight: {
        marginRight: theme.spacing(1)
    },
    marginTop: {
        marginTop: theme.spacing(1)
    },
    marginBottom: {
        marginBottom: theme.spacing(1)
    },
    margin: {
        margin: theme.spacing(1)
    },
});