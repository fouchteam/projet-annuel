import axios from 'axios';
import { urlConstants } from "../../_constants/url.constants";

const DEFAULT_TIMEOUT = 5000;

const instance = axios.create({
    baseURL: urlConstants.API_BASE_URL,
    timeout: DEFAULT_TIMEOUT,
    headers: {
        'Access-Control-Allow-Origin': "*",
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: "Bearer " + localStorage.getItem('token')
    },
    withCredentials: true
});
export default instance;
