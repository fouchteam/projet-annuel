import React, { useContext, useEffect } from "react";
import { customHistory } from "../_helper/history";
import { endpointConstants } from "../_constants/endpoint.constants";
import { AuthContext } from "../Context/AuthContext";

// TODO If possible transform this HOC into a Hook HOC
export default (ComposedComponent) => (props) => {
  // Auth context
  const authContext = useContext(AuthContext);

  // Push to login route if not authenticated on mount and update
  useEffect(() => {
    if (!authContext.isLogged && !localStorage.getItem("token"))
      customHistory.push(endpointConstants.LOGIN_ENDPOINT);
  }, [authContext.isLogged]);

  return <ComposedComponent {...props} />;
};
